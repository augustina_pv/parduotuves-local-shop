<?php

use Illuminate\Database\Seeder;

class PvmGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pvm_groups')->insert([
            [
                'title' => 'PVM',
                'value' => 21.00,
                'created_at' => new \DateTime()
            ],
            [
                'title' => 'NOPVM',
                'value' => 5.00,
                'created_at' => new \DateTime()
            ]
        ]);
    }
}
