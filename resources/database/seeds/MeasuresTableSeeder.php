<?php

use Illuminate\Database\Seeder;

class MeasuresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('measures')->insert([
            [
                'title' => 'kg',
                'created_at' => new \DateTime()                
            ],
            [
                'title' => 'cm',
                'created_at' => new \DateTime()                
            ],
            [
                'title' => 'litrai',
                'created_at' => new \DateTime()                
            ]            
        ]);
    }
}
