<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(RolesTableSeeder::class);

        $this->call(PvmGroupsTableSeeder::class);
        $this->call(MeasuresTableSeeder::class);

        $this->call(CategoriesTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(CategoryProductTableSeeder::class);
        
        

        $this->call(SuppliersTableSeeder::class);

        $this->call(TypesTableSeeder::class);

        $this->call(PermissionsTableSeeder::class);
    }
}
