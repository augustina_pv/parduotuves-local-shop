<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            [
                'title' => 'Limonadas',
                'sku' => '1111111111',
                'barcode' => '84545463445',
                'price' => '0.99',
                'price_purchase' => '0.79',
                'pvm_group_id'  => 1,
                'measure_id'  => 3,
                'image' =>'products\rwHUupVDuslHuZlueZOK5vGJ5JJSreL6QGVZCZxy.png'
            ],
            [
                'title' => 'Coca Cola',
                'sku' => '222222222222',
                'barcode' => '84545222222',
                'price' => '0.79',
                'price_purchase' => '0.59',
                'pvm_group_id'  => 1,
                'measure_id'  => 3,
                'image' => 'products\R8JwYMyIBj1cqN0TCUonOkzToEwG2M7zWi0ueYVl.jpeg'
            ],
            [
                'title' => 'Vistiena',
                'sku' => '333333333334444',
                'barcode' => '845454333333',
                'price' => '2.50',
                'price_purchase' => '2.00',
                'pvm_group_id'  => 1,
                'measure_id'  => 1,
                'image' => 'products\FITtnc4T74deGV4wqcxvYieBKwCPC2xZTrLxpvUE.jpeg'
            ],
            [
                'title' => 'Snickers',
                'sku' => '535463464654',
                'barcode' => '84545111111',
                'price' => '1.19',
                'price_purchase' => '0.99',
                'pvm_group_id'  => 1,
                'measure_id'  => 2,
                'image' => 'products\yDb0MBwAIbE7ZbTyYwWWaHRv8INfrvkxgWKhFYDa.jpeg'
            ],
            [
                'title' => 'Pyragas',
                'sku' => '566666666666',
                'barcode' => '84547877878',
                'price' => '5.99',
                'price_purchase' => '4.99',
                'pvm_group_id'  => 1,
                'measure_id'  => 1,
                'image' => 'products/H5Ulu4EpvQbM8tLziHiaZI761hnU89ehskOes8XP.jpeg'
            ],
            [
                'title' => 'Keksas',
                'sku' => '9999999999999',
                'barcode' => '89798788998',
                'price' => '3.99',
                'price_purchase' => '3.19',
                'pvm_group_id'  => 1,
                'measure_id'  => 1,
                'image' => 'products/D3YehDbLh9GnWZLiFqb973bzOQONokZW4a1e01ms.jpeg'
            ],
            [
                'title' => 'Agurkai',
                'sku' => '9999999999999',
                'barcode' => '020505000505',
                'price' => '0.99',
                'price_purchase' => '0.50',
                'pvm_group_id'  => 1,
                'measure_id'  => 1,
                'image' => 'products/pmVkXQBYNfgLXO2aLfF2a1l7UniMUpiVKBnIo9ZZ.jpeg'
            ],
            [
                'title' => 'Pomidorai',
                'sku' => '888888888888',
                'barcode' => '89798788888877',
                'price' => '1.20',
                'price_purchase' => '0.80',
                'pvm_group_id'  => 1,
                'measure_id'  => 1,
                'image' => 'products/F6CUQtDdDQtSmY8S6IcOZO0jarFULaE9Utbz2AHa.jpeg'
            ],
            [
                'title' => 'Bananai',
                'sku' => '87987987987',
                'barcode' => '1111118888877',
                'price' => '0.79',
                'price_purchase' => '0.50',
                'pvm_group_id'  => 1,
                'measure_id'  => 1,
                'image' => 'products/1PFlsZICEtJxnb1cU6SXTWjlk0S4RYXhE3IbQPss.png'
            ],
            [
                'title' => 'Duona Bociu',
                'sku' => '22227987987',
                'barcode' => '99999118888877',
                'price' => '1.00',
                'price_purchase' => '0.60',
                'pvm_group_id'  => 1,
                'measure_id'  => 1,
                'image' => 'products/Z1ZKpK8WJlgTVDfvuGQN4rTpcpKLZdqU024CaKrK.jpeg'
            ],
            [
                'title' => 'Kiauliena kiniskai',
                'sku' => '555527987987',
                'barcode' => '565659118888877',
                'price' => '5.50',
                'price_purchase' => '3.50',
                'pvm_group_id'  => 2,
                'measure_id'  => 1,
                'image' => 'products/ZzIZYg9D5ZUjQZpEyp1oNnMYQ07EZJsuMdmSmx1Y.jpeg'
            ],  
            [
                'title' => 'Pienas Mu',
                'sku' => '555527987987',
                'barcode' => '565659118888877',
                'price' => '1.10',
                'price_purchase' => '0.50',
                'pvm_group_id'  => 1,
                'measure_id'  => 3,
                'image' => 'products/CzjeTfRt5Ew6otIEHAxb7tlVsqO6T0NSkNPwBaxc.png'
            ],                       
        ]);     
    }
}
