<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            [
                'title' => 'View Goods Receiving',
                'name'  => 'view-goods-receiving',
                'created_at' => new \DateTime()
            ],
            [
                'title' => 'View Transactions',
                'name'  => 'view-transactions',
                'created_at' => new \DateTime()
            ],
            [
                'title' => 'View Types',
                'name'  => 'view-types',
                'created_at' => new \DateTime()
            ],
            [
                'title' => 'View Products',
                'name'  => 'view-products',
                'created_at' => new \DateTime()
            ],
            [
                'title' => 'View Categories',
                'name'  => 'view-categories',
                'created_at' => new \DateTime()
            ],
            [
                'title' => 'View Pvm Groups',
                'name'  => 'view-pvm-groups',
                'created_at' => new \DateTime()
            ],
            [
                'title' => 'View Measures',
                'name'  => 'view-measures',
                'created_at' => new \DateTime()
            ],            
            [
                'title' => 'View Suppliers',
                'name'  => 'view-suppliers',
                'created_at' => new \DateTime()
            ],
            [
                'title' => 'View Supplier Documents',
                'name'  => 'view-supplier-documents',
                'created_at' => new \DateTime()
            ],
            [
                'title' => 'View Customers',
                'name'  => 'view-customers',
                'created_at' => new \DateTime()
            ],
            [
                'title' => 'View Inventory',
                'name'  => 'view-inventory',
                'created_at' => new \DateTime()
            ],
            [
                'title' => 'View Receipts',
                'name'  => 'view-receipts',
                'created_at' => new \DateTime()
            ],
            [
                'title' => 'View Orders',
                'name'  => 'view-orders',
                'created_at' => new \DateTime()
            ],            
            [
                'title' => 'View Inner Documents',
                'name'  => 'view-inner-documents',
                'created_at' => new \DateTime()
            ],
            [
                'title' => 'View Users',
                'name'  => 'view-users',
                'created_at' => new \DateTime()
            ],
            [
                'title' => 'View Roles',
                'name'  => 'view-roles',
                'created_at' => new \DateTime()
            ],
            [
                'title' => 'View Permissions',
                'name'  => 'view-permissions',
                'created_at' => new \DateTime()
            ],
            [
                'title' => 'View Reports',
                'name'  => 'view-reports',
                'created_at' => new \DateTime()
            ]            
        ]);

        DB::table('user_permission')->insert([
            [
                'user_id' => 1,
                'permission_id' => 1,
            ],
            [
                'user_id' => 1,
                'permission_id' => 2,
            ],
            [
                'user_id' => 1,
                'permission_id' => 3,
            ],
            [
                'user_id' => 1,
                'permission_id' => 4,
            ],
            [
                'user_id' => 1,
                'permission_id' => 5,
            ],
            [
                'user_id' => 1,
                'permission_id' => 6,
            ],
            [
                'user_id' => 1,
                'permission_id' => 7,
            ],
            [
                'user_id' => 1,
                'permission_id' => 8,
            ],
            [
                'user_id' => 1,
                'permission_id' => 9,
            ],
            [
                'user_id' => 1,
                'permission_id' => 10,
            ],
            [
                'user_id' => 1,
                'permission_id' => 11,
            ],
            [
                'user_id' => 1,
                'permission_id' => 12,
            ],
            [
                'user_id' => 1,
                'permission_id' => 13,
            ],
            [
                'user_id' => 1,
                'permission_id' => 13,
            ],
            [
                'user_id' => 1,
                'permission_id' => 14,
            ],
            [
                'user_id' => 1,
                'permission_id' => 15,
            ],
            [
                'user_id' => 1,
                'permission_id' => 16,
            ],
            [
                'user_id' => 1,
                'permission_id' => 17,
            ],             
            [
                'user_id' => 1,
                'permission_id' => 18,
            ],             
        ]);
    }
}
