<?php

return [

	//index
	'cancel' => 'Atšaukti',
	'customer' => 'Klientas',
	'pay' => 'Mokėti',
	'card_accepted' => 'Koretelė priimta',
	'search_clean' => 'Išvalyti',
	'products' => 'Produktai',
	'search_products' => 'ieškoti produktų',

	//payment
    'back' => 'Atgal',
    'payment' => 'Apmokėjimas',
    'cash' => 'GRYNAIS',
    'card' => 'KORTELE',
    'gift_coupon' => 'Dovanų kuponas',
    'credit' => 'KREDITAS',
    'receipt' => 'Sąskaita',
    'receipt_by_mail' => 'Čekis el. paštu',
    'total_price' => 'Mokėtina suma',
    'change' => 'GRĄŽA',	
	
];