<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'login.message' => 'Prisijunkite, norėdami pradėti darbą', //Sign in to start your session

    'email' => 'El. Paštas', //Email
    'password' => 'Slaptažodis', //Password
    'confirm_password' => 'Patvirtinkite Slaptažodį', //Confirm Password

    'agree_to_terms' => 'Sutinku su <a href="#">sąlygom</a>', //I agree to the <a href="#">terms</a>

    'login.sign_in' => 'Prisijunkite', //Sign In

    'login.remember_me' => 'Prisiminti mane', //Remember Me
    'login.forgot_password' => 'Pamiršau Slaptažodį', //I forgot my password
    'login.register' => 'Registruotis', //Register a new membership


    'register.message' => 'Registruokitės', //Register a new membership
    'register.name' => 'Vardas', //Name
    'register.lastname' => 'Pavardė', //Last Name
    'register.register' => 'Registruotis', //Register

    'register.have_a_membership' => 'Esu prisiregistravęs, prisijungti', //I already have a membership

    

];
