const cartModule = {

	namespaced: true,

	state: {
		//products: [],
		products: [],
		last_added_product: {},
		//selected_product: {},
	},

	/*
	product: {
		id
		title
		sku
		barcode
		price
		price_purchase
		price_pvm
		price_total
		limit - nenaud.
		count - default 1
		image
		pvm
		pvm_group
		pvm_group_id
		measure_id - nenaud
		created_at - nenaud
		deleted_at - nenaud
		updated_at - nenaud
	},
	*/

	getters: {

		products(state){
			return state.products;
		},
		last_added_product(state){
			return state.last_added_product;
		},
		/*
		selected_product(state){
			return state.selected_product;
		},
		*/
	},

	mutations: {

		fetch_cart_products(state, cart_products){
            state.products = cart_products;
		},
		add_product(state, product){
			//console.log('cart/add_product');

			var product_id = product.id;
        	var rado=false;

        	state.products.forEach(function(cart_product, key){
        		if(cart_product.id == product_id){
        			rado = true;

        			//cart_product.price = product.price;
        			cart_product.count++;
        			Vue.set(cart_product, 'count', cart_product.count);

        			state.last_added_product = cart_product;
        		}
        	});

        	if(!rado){
        		product.count = 1;
        		state.products.push(product);
        		state.last_added_product = product;
        	}
        	//console.log(state.last_added_product);
		},
		remove_product(state, product){
			var product_id = product.id;
        	var rado=false;

        	state.products.forEach(function(cart_product, key){
        		if(cart_product.id == product_id){
        			rado = true;
        			cart_product.price = product.price;
        			cart_product.count--;

        			if(cart_product.count < 1)
        				state.products.splice(key, 1);
        		}
        	});

        	if(!rado){
        		//product.count = 1;
        		state.products.push(product);
        	}			
		},

		remove_all_products(state){
			state.products = [];
		},

		/*
        empty_cart(){
            axios.delete('empty_cart').then((res) => {
                console.log(res.data);
            });         
        },  
        */
        add_product_discount_percentage(state, percentage){
        	//console.log(percentage);
        	state.products.forEach(function(product){
        		if(product.selected == 'selected'){

                    Vue.set(product, 'discount_percentage', percentage);
                    Vue.set(product, 'price_discounted', null);  

                    //state.selected_product =  product;     			
        		}
        	});
        },
        select_product(state, product_id){
        	state.products.forEach(function(product){
        		if(product.id == product_id){
        			if(product.selected == 'selected')
	        			Vue.set(product, 'selected', '');
	        		else
	        			Vue.set(product, 'selected', 'selected');    			
        		}
        		else{
        			//Vue.set(product, 'selected', '');
        		}
        	});
        }    
        	
	},

	actions: {
		fetch_cart_products(context, user_id){
			//var user_id = 1;
			var app = this;
            return axios.get('get_cart_products/' + user_id).then((res) => {			
				context.commit('fetch_cart_products', res.data)
			})
	        .catch(error => {
	            console.log('Klaida, bandykite dar kartą');
	            console.log(error);
	        });
		},
		add_product(context, product){
            var cart_product = {
                id: product.id,
                title: product.title,
                price: product.price,
                price_pvm: product.price_pvm,
                pvm_group_id: product.pvm_group_id,
                discount_percentage: null,
                price_discounted: null,
                selected: "",
                count: 1
            }
			//console.log(cart_product)
			context.commit('add_product', cart_product)
		},
		/*
		add_product_test_promise(context, product){
		    return new Promise((resolve, reject) => {
				//context.commit('add_product', cart_product)
		    	resolve(cart_product) //grazina cart_product
		    })
		},	
		*/	
		remove_product(context, product){
			context.commit('remove_product', product)
		},	
		remove_all_products(context){
			context.commit('remove_all_products')
		},			
		/*
		empty_cart(context){
			context.commit('empty_cart')
		},
		*/
		add_product_discount_percentage(context, percentage){
			context.commit('add_product_discount_percentage', percentage)
		},
		select_product(context, product_id){
			context.commit('select_product', product_id)
		}
	}
}


export default cartModule