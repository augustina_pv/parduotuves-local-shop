const productsListModule = {

	namespaced: true,

	state: {
		products: [],
		search: '',
	},

	getters: {
		products(state){
            return state.products
                .filter((product) => {

                    return  product.title.toUpperCase().match(state.search.toUpperCase()) 
                            || product.sku.match(state.search)
                            || (product.barcode != null && product.barcode.match(state.search) )

                });			
		},
		search(state){
			return state.search;
		}
	},

	mutations: {
		fetch_products(state, products){
            state.products = products;
		},
		fetch_products_by_category(state, products){
            state.products = products;
        },
        search(state, value){
        	state.search = value;
        }
	},

	actions: {
		fetch_products(context){
			return axios.get('get_products').then((res) => {
				context.commit('fetch_products', res.data);
			})
			.catch(error => {
	            console.log('Klaida, bandykite dar kartą');
	            console.log(error);
	        }); 			
		},
		fetch_products_by_category(context, category_id){
			return axios.get('get_products_by_category/' + category_id).then((res) => {
				context.commit('fetch_products_by_category', res.data);
			})
			.catch(error => {
	            console.log('Klaida, bandykite dar kartą');
	            console.log(error);
	        }); 				
		},
		search(context, value){
			context.commit('search', value)
		},
	}
}


export default productsListModule