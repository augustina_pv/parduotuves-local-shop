const breadcrumbModule = {

	namespaced: true,

	state: {
		//parent: 0, //category_id
		breadcrumb: [],
	},

	getters: {
		breadcrumb(state){
			return state.breadcrumb;
		}
	},

	mutations: {
		fetch_breadcrumb(state, breadcrumb){
            breadcrumb.reverse();
            state.breadcrumb = breadcrumb;               
		}
	},

	actions: {
		fetch_breadcrumb(context, parent=0){
			return axios.get('get_breadcrumb/' + parent).then((res) => {
				context.commit('fetch_breadcrumb', res.data);
			})
			.catch(error => {
	            console.log('Klaida, bandykite dar kartą');
	            console.log(error);
	        }); 
		}
	}
}


export default breadcrumbModule