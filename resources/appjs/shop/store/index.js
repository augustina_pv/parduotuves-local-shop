import Vue from 'vue'
import Vuex from 'vuex'
import breadcrumbModule from './modules/breadcrumbModule'
import categoriesModule from './modules/categoriesModule'
import productsListModule from './modules/productsListModule'
import cartModule from './modules/cartModule'

Vue.use(Vuex)

export default new Vuex.Store({

	modules: {
		breadcrumb: breadcrumbModule,
		categoriesList: categoriesModule,
		productsList: productsListModule,
		cart: cartModule,
	},

})