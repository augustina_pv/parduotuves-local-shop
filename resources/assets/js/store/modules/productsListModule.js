const productsListModule = {

	namespaced: true,

	state: {
		products: [],
		search: '',
	},

	/*
	product: {
		id
		title
		sku
		barcode
		price
		price_purchase
		price_pvm
		price_total
		limit - nenaud.
		count
		image
		pvm
		pvm_group
		pvm_group_id
		measure_id
		created_at
		deleted_at
		updated_at
	},
	*/

	getters: {
		products(state){
			//return state.products;

            return state.products
                .filter((product) => {

                    return  product.title.toUpperCase().match(state.search.toUpperCase()) 
                            || product.sku.match(state.search)
                            || (product.barcode != null && product.barcode.match(state.search) )

                });			
		},
		search(state){
			return state.search;
		}
	},

	mutations: {
		fetch_products(state){
            axios.get('get_products').then((res) => {
                state.products = res.data;
                //console.log(res.data);
            }); 
		},
		fetch_products_by_category(state, category_id){
            axios.get('get_products_by_category/' + category_id).then((res) => {
                state.products = res.data;
            }); 
        },
        search(state, value){
        	state.search = value;
        }
	},

	actions: {
		fetch_products(context){
			context.commit('fetch_products')
		},
		fetch_products_by_category(context, category_id){
			context.commit('fetch_products_by_category', category_id)
		},
		search(context, value){
			context.commit('search', value)
		},
	}
}


export default productsListModule