const categoriesModule = {

	namespaced: true,

	state: {
		parent: 0,
		categories: [],
	},

	/*
	category: {
		id
		title
		parent_id
		image
		ordering
		created_at - nenaud.
		deleted_at - nenaud.
		updated_at - nenaud.
	},
	*/

	getters: {
		parent(state){
			return state.parent
		},

		categories(state){
			return state.categories;
		}
	},

	mutations: {
		fetch_categories(state, parent){
			state.parent = parent;
            axios.get('get_categories/' + state.parent).then((res) => {
                state.categories = res.data;
                //console.log(res.data)
            });               
		}
	},

	actions: {
		fetch_categories(context, parent=0){
			context.commit('fetch_categories', parent)
		}
	}
}


export default categoriesModule