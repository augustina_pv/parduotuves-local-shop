
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import store from './store/index' 

window.Vue = require('vue');

Vue.component('breadcrumb', require('./components/shop/BreadcrumbComponent.vue'));
Vue.component('categories', require('./components/shop/CategoriesComponent.vue'));
Vue.component('products', require('./components/shop/ProductsComponent.vue'));
Vue.component('cart', require('./components/shop/CartComponent.vue'));
Vue.component('calculator', require('./components/shop/CalculatorComponent.vue'));
Vue.component('checkout', require('./components/shop/CheckoutComponent.vue'));


const point_of_sale_app = new Vue({
    el: '#point-of-sale',
    delimiters: ['<%','%>'], 
    store,
    mounted() {
        console.log('Test App started');
        
        this.$store.dispatch('categoriesList/fetch_categories'); //api
        this.$store.dispatch('productsList/fetch_products'); //api
        this.$store.dispatch('cart/fetch_cart_products'); //api

        var app = this;
        $('#payment-modal-id').on("hidden.bs.modal", function(){
            //console.log('hide modal');
            app.show_payment_window_full();
        });
    },    
    data: {
        //productss: this.$store.getters['productsList/products'],
    	//cart: [],
        //breadcrumb: []
        //searchh: ''
        order_processing: false,
        message_after_order: ''
    },
    computed:{
        search:{
            get(){
                return this.$store.getters['productsList/search'];
            },
            set(value){
                this.$store.dispatch("productsList/search", value );
            }
        },
        /*
        price_total: function(){
            return 10;
        },
        */
        total_products_price(){
            var price_total = 0
            var app = this;
            var products = this.$store.getters['cart/products'];
            //console.log(products);

            products.forEach(function(product){
                var price = app.product_discount_filter(product);
                //var price = product.price;

                price_total += product.count * price;
            }); 
            
            return price_total.toFixed(2);
        },         
    },
    methods: {
        testas(){
            console.log('testas'); 
        },        
        test_click(){
            //var search = this.$store.getters['productsList/search'];
            //console.log(search);
            //var prods = this.$store.getters['productsList/products']
            //this.$store.dispatch('cart/add_product');
        },

        //--------------- main -------------------------------
        reload_screen(){
            axios.delete('empty_cart').then((res) => {
                console.log(res.data);

                this.$store.dispatch('cart/remove_all_products'); //api
                this.$store.dispatch('categoriesList/fetch_categories'); //api
                this.$store.dispatch('productsList/fetch_products'); //api
                this.$store.dispatch('cart/fetch_cart_products'); //api                 
            });  
           
        },
        clear_search(){
            //this.redirect_if_not_logged();
            this.search = '';
        },
        show_payment_window_change(){
            $('#payment-modal-id .payment-left').addClass('hidden');
            $('#payment-modal-id .price-total').addClass('hidden');
            $('#payment-modal-id .price-change').addClass('hidden');
            $('#payment-modal-id .price-change-after').removeClass('hidden');
            $('#payment-modal-id .payment-row').addClass('hidden');
        },

        show_payment_window_full(){
            $('#payment-modal-id .payment-left').removeClass('hidden');
            $('#payment-modal-id .price-total').removeClass('hidden');
            $('#payment-modal-id .price-change').removeClass('hidden');
            $('#payment-modal-id .price-change-after').addClass('hidden');            
            $('#payment-modal-id .payment-row').removeClass('hidden');
        },         
        product_discount_filter(product){
            var price = product.price;

            if(product.price_discounted)
                price = product.price_discounted;

            if(product.discount_percentage){
                var discount = ( parseFloat(product.price) * parseFloat(product.discount_percentage) ) / 100;
                var new_price = product.price - discount;
                new_price = new_price.toFixed(2);
                price = new_price;                    
            }

            return price;
        },              
        change_category(category_parent_id){
            if(category_parent_id == 0){ //home
                this.$store.dispatch('breadcrumb/fetch_breadcrumb'); //api
                this.$store.dispatch('categoriesList/fetch_categories'); //api
                this.$store.dispatch('productsList/fetch_products'); //api
            }
            else{
                this.$store.dispatch('breadcrumb/fetch_breadcrumb', category_parent_id);
                this.$store.dispatch('categoriesList/fetch_categories', category_parent_id);
                this.$store.dispatch('productsList/fetch_products_by_category', category_parent_id);                
            }            
        },
        //---------------------------------------------------

        //---------- breadcrumb ---------------------------

        on_click_go_home(){
            this.$store.dispatch('categoriesList/fetch_categories'); //api
        },
        on_breadcrumb_click(category_id){
            console.log(category_id);
            this.change_category(category_id);
        },    
        //------------------------------------------------    

        //-------- products list -----------------------
        on_product_click(product){
            var app = this;
            this.$store.dispatch('cart/add_product', product).then(function(){
                //console.log('promise');
                var last_added_product = app.$store.getters['cart/last_added_product'];
                //console.log(last_added_product);

                app.api_post_send('save_to_cart', {product: product});
            });
        },
        //----------------------------------------------

        //--------- categories --------------------------
        on_category_click(category){
            console.log(category.id);
            this.change_category(category.id);
        },
        //-----------------------------------------------

        //---------- cart -----------------
        on_add_product(product){
            console.log(product);
            this.$store.dispatch('cart/add_product', product);
            this.api_post_send('save_to_cart', {product: product});
        },
        on_remove_product(product){
            
            if (product.count === 1){
                this.api_post_send('delete_product_from_cart', {product: product});
            }
            else{
                this.api_post_send('save_to_cart', {product: product});
            }

            this.$store.dispatch('cart/remove_product', product);
        },
        on_cart_item_click(product){ //api
            console.log(product.id);
            var app = this;
            this.$store.dispatch('cart/select_product', product.id).then(function(){
                var products = app.$store.getters['cart/products'];
                console.log(products);
            });
        },        

        api_post_send(action, data){
            
            axios.post(action, data).then((res) => {
                console.log(res);
                console.log('make sync!!!');
            })
            .catch(error => {
                //alert('Klaida, bandykite dar kartą');
                console.log(error);
            });     
        },                
        //---------------------------------

        //---------- calculator --------------
        on_reload_screen(){
            console.log('reload_screen');
            this.reload_screen();
        },
        on_product_discount_percentage(percentage){
            //console.log(percentage);
            var app = this;
            this.$store.dispatch('cart/add_product_discount_percentage', percentage).then(function(){
                var selected_product = app.$store.getters['cart/selected_product'];
                console.log(selected_product);
                app.api_post_send('save_to_cart', {product: selected_product});
            });
        },
        on_open_checkout(){
            console.log('open checkout');
            this.$store.dispatch('cart/fetch_cart_products'); //api
        },
        //-------------------------------------

        //---------- checkout -------------------
        on_save_order(order) {
            
            var app = this;
            app.order_processing = true;
            app.show_payment_window_change();
            order.products = this.$store.getters['cart/products'];
            console.log(order);

            axios.post('save_orders', order).then((res) => {

                console.log(res);

                if(res.status == 200){

                    app.message_after_order = '<div class="alert alert-success">'+
                                        '<strong>Mokėjimas įvykdytas</strong>'+
                                   '</div>';

                    setTimeout(function(){
                        app.message_after_order = '';
                    }, 5000);


                    app.order_processing = false;
                    this.reload_screen();
                }
            })
            .catch(error => {
                alert('Klaida, bandykite dar kartą');
                //console.log(error);
            });
        },         

        //----------------------------------------
    },
    watch: {
    	//cart: {
    	//	handler: function(val, oldVal){
    	//		console.log('cart updated');
    	//	},
    	//	deep: true
    	//}
    }

});