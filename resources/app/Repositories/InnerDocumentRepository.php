<?php

namespace App\Repositories;

use App\Models\InnerDocument;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class InnerDocumentRepository
 * @package App\Repositories
 * @version November 24, 2017, 10:11 am UTC
 *
 * @method InnerDocument findWithoutFail($id, $columns = ['*'])
 * @method InnerDocument find($id, $columns = ['*'])
 * @method InnerDocument first($columns = ['*'])
*/
class InnerDocumentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'file',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return InnerDocument::class;
    }
}
