<?php

namespace App\Repositories;

use App\Models\Receipt;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ReceiptRepository
 * @package App\Repositories
 * @version November 24, 2017, 9:45 am UTC
 *
 * @method Receipt findWithoutFail($id, $columns = ['*'])
 * @method Receipt find($id, $columns = ['*'])
 * @method Receipt first($columns = ['*'])
*/
class ReceiptRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer_id',
        'price'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Receipt::class;
    }
}
