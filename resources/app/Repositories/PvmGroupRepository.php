<?php

namespace App\Repositories;

use App\Models\PvmGroup;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PvmGroupRepository
 * @package App\Repositories
 * @version November 23, 2017, 9:51 am UTC
 *
 * @method PvmGroup findWithoutFail($id, $columns = ['*'])
 * @method PvmGroup find($id, $columns = ['*'])
 * @method PvmGroup first($columns = ['*'])
*/
class PvmGroupRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PvmGroup::class;
    }
}
