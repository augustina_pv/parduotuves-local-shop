<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
	//public $appends = ['pos_pvm_option'];

    public function discounts()
    {
    	//cart - gali tureti viena discount ( $cart->discounts->last() )
        return $this->morphMany('App\Models\Discount', 'object');
    }

    public function pvm_group(){
        return $this->belongsTo('App\Models\PvmGroup');
    }

	/*
    public function getPosPvmOptionAttribute(){
    	return $this->pvm_group->pos_option;
    	//return 5;
    }
    */
}
