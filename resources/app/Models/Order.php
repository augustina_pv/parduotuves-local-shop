<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function items(){
    	return $this->hasMany('App\Models\OrderItem');
    }

    public function transactions()
    {
        //iesko db transactions, pagal document_id ir document_type
        return $this->morphMany('App\Models\Transaction', 'document');
    }     
     
}
