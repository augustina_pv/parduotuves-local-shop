<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Receipt
 * @package App\Models
 * @version November 24, 2017, 9:45 am UTC
 *
 * @property string customer_id
 * @property float price
 */
class Receipt extends Model
{
    use SoftDeletes;

    public $table = 'receipts';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'customer_id',
        'price'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'customer_id' => 'integer',
        'price' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'customer_id' => 'required',
        'price' => 'required'
    ];

    public function customer(){
        return $this->belongsTo('App\Models\Customer');
    }

    
}
