<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InventoryActivity extends Model
{
    public function items(){
    	return $this->hasMany('App\Models\InventoryActivitiesItem');
    }

    public function user(){
    	return $this->belongsTo('App\Models\User');
    }    
}
