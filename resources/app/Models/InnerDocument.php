<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class InnerDocument
 * @package App\Models
 * @version November 24, 2017, 10:11 am UTC
 *
 * @property string file
 * @property string description
 */
class InnerDocument extends Model
{
    use SoftDeletes;

    public $table = 'inner_documents';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'file',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'file' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
