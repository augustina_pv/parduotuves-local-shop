<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Category
 * @package App\Models
 * @version November 21, 2017, 12:13 pm UTC
 *
 * @property string title
 * @property integer parent_id
 * @property string image
 * @property integer ordering
 */
class Category extends Model
{
    use SoftDeletes;

    public $table = 'categories';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'title',
        'parent_id',
        'image',
        'ordering'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'title' => 'string',
        'parent_id' => 'integer',
        'image' => 'string',
        'ordering' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required',
        //'ordering' => 'integer'
    ];


    public function childs() {
        return $this->hasMany('App\Models\Category', 'parent_id', 'id')->orderBy('ordering', 'asc') ;
    }

    public function products() {
        return $this->belongsToMany('App\Models\Product');
    }  

    public function user_activities()
    {
        //iesko db user_activities, pagal table_id ir table_type
        return $this->morphMany('App\Models\UserActivity', 'table');
    }  

    
}
