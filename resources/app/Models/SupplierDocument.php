<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SupplierDocument
 * @package App\Models
 * @version November 23, 2017, 12:10 pm UTC
 *
 * @property string nr
 * @property integer supplier_id
 */
class SupplierDocument extends Model
{
    use SoftDeletes;

    public $table = 'supplier_documents';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'nr',
        'date',
        'supplier_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nr' => 'string',
        'date' => 'dateTime',
        'supplier_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nr' => 'required',
        'date' => 'required',
    ];

    public function supplier(){
        return $this->belongsTo('App\Models\Supplier');
    }

    public function transactions()
    {
        //iesko db transactions, pagal document_id ir document_type
        return $this->morphMany('App\Models\Transaction', 'document');
    }    

    
}
