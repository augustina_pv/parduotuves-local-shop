<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Models\Product;
use App\Models\InventoryActivity;
use Carbon\Carbon;

class ReportsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware('can:view-reports');
    }

    public function reports(Request $request){

    	//$transactions = Transaction::all();
        $transactions = Transaction::datesFilter([$request->nuo, $request->iki])->typesFilter($request->tipas)->get();

    	$report_products = [];
        $total_count = 0;
        $total_price = 0;
    	//$total = 0;


        //--- generate $report_products from transactions
    	foreach ($transactions as $transaction) {
    		
            $show_product = true;

    		if($show_product){

	    		$found_product = false;
	    		$total = $transaction->price * $transaction->count;
	    		
	    		foreach ($report_products as $key => $product) {
	    			if($transaction->product_id === $product['product_id'] && $transaction->price === $product['price']){
	    				$report_products[$key]['count'] += $transaction->count; 
	    				$report_products[$key]['total'] += $total;
	    				$found_product = true;

	    			}
	    		}

                //----------------- product title & is deleted ----------------
                $product_title = $transaction->getProductTitle();
                $product_sku = $transaction->product ? $transaction->product->sku : 'Ištrintas';
                $product_sku = $product_title ? $product_sku : ''; 
                $product_title = $product_title ? $product_title : 'Nerasta DB';
                $product_deleted = $transaction->product ? false : true;
                $product_deleted = $product_title ? $product_deleted : false;
                //-------------------------------------

	    		if(!$found_product){ 
		    		$report_products[] = [
		    			'product_id' => $transaction->product_id,
		    			'title' => $product_title,
		    			'sku' => $product_sku,
		    			'count' => $transaction->count,
		    			'price' => $transaction->price,
		    			'total' => $total,
		    			'type' => $transaction->type->name,
		    			'user' => $transaction->user->name,
		    			'created_at' => $transaction->created_at->format('Y-m-d'),
                        'deleted' => $product_deleted,
		    		];    			
	    		}

                $total_count += $transaction->count;
                $total_price += $total;
    		}

    	} //end generate $report_products


    	//var_dump($report_products);

        //-------------------- is type checked --------------------------
        $pirkimas_checkbox = '';
        $pardavimas_checkbox = '';

        if(isset($request->tipas)){
            $filter_by_type = $request->tipas;

            foreach ($filter_by_type as $type) {
                if($type == 1)
                    $pirkimas_checkbox = 'checked';
                if($type == 2)
                    $pardavimas_checkbox = 'checked';
            }
        }
        //---------------------------------------------------------------

    	//------------------------- dates inputs ------------------------
    	$date_from = $request->nuo ? $request->nuo : $this->date_start();
    	$date_to = $request->iki ? $request->iki : $this->date_end();
    	//---------------------------------------------------------------

    	$data = [
    		'report_products' => $report_products,
            'total_count' => $total_count,
            'total_price' => $total_price,
    		'date_from' => $date_from,
    		'date_to' => $date_to,
    		'pirkimas_checkbox' => $pirkimas_checkbox,
    		'pardavimas_checkbox' => $pardavimas_checkbox,
    	];

    	return view('admin.reports.reports', $data);
    }

    public function reports_purchase(Request $request){

        return $this->view_reports_type($request, 1); //purchase
    }

    public function reports_sale(Request $request){
        return $this->view_reports_type($request, 2); //sale
    }    


    private function date_start(){
    	$date_start = new Carbon('first day of this month');
    	$date_start = strtotime($date_start->addWeeks(0));
    	$date_start = date('Y-m-d', $date_start);

    	return $date_start;
    }

    private function date_end(){
    	$date_end = new Carbon('last day of this month');
    	$date_end = strtotime($date_end->addWeeks(0));
    	$date_end = date('Y-m-d', $date_end);     
    	
    	return $date_end;
    }

    private function view_reports_type(Request $request, $type_id){

        $transactions = Transaction::datesFilter([$request->nuo, $request->iki])->where('type_id', $type_id)->get();
        $report_products = [];
        $total_count = 0;
        $total_price = 0;

        //--- generate $report_products from transactions
        foreach ($transactions as $transaction) {
            
            $show_product = true;

            if($show_product){

                $found_product = false;
                $total = $transaction->price * $transaction->count;
                
                foreach ($report_products as $key => $product) {
                    if($transaction->product_id === $product['product_id'] && $transaction->price === $product['price']){
                        $report_products[$key]['count'] += $transaction->count; 
                        $report_products[$key]['total'] += $total;
                        $found_product = true;

                    }
                }

                //----------------- product title & is deleted ----------------
                $product_title = $transaction->getProductTitle();
                $product_sku = $transaction->product ? $transaction->product->sku : 'Ištrintas';
                $product_sku = $product_title ? $product_sku : ''; 
                $product_title = $product_title ? $product_title : 'Nerasta DB';
                $product_deleted = $transaction->product ? false : true;
                $product_deleted = $product_title ? $product_deleted : false;
                //-------------------------------------

                if(!$found_product){ 
                    $report_products[] = [
                        'product_id' => $transaction->product_id,
                        'title' => $product_title,
                        'sku' => $product_sku,
                        'count' => $transaction->count,
                        'price' => $transaction->price,
                        'total' => $total,
                        'type' => $transaction->type->name,
                        'user' => $transaction->user->name,
                        'created_at' => $transaction->created_at->format('Y-m-d'),
                        'deleted' => $product_deleted,
                    ];              
                }

                $total_count += $transaction->count;
                $total_price += $total;
            }

        } //end generate $report_products


        //var_dump($report_products);

        //------------------------- dates inputs ------------------------
        $date_from = $request->nuo ? $request->nuo : $this->date_start();
        $date_to = $request->iki ? $request->iki : $this->date_end();
        //---------------------------------------------------------------

        $data = [
            'report_products' => $report_products,
            'total_count' => $total_count,
            'total_price' => $total_price,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'type_id' => $type_id
        ];

        return view('admin.reports.reports_type', $data);        
    }

    public function reports_inventory(){

        $inventory_activities = InventoryActivity::all();
        $data = [
            'inventory_activities' => $inventory_activities
        ];

        return view('admin.reports.reports_inventory', $data);
    }

    public function reports_inventory_show($id){
        $inventory_activity = InventoryActivity::find($id);
        $shortage_total_count = 0;
        $shortage_total_price = 0;

        foreach ($inventory_activity->items as $item) {
            $shortage_total_count += $item->shortage;
            $shortage_total_price += $item->shortage_total;
        }

        //var_dump($shortage_total_count);
        //var_dump($shortage_total_price);

        $data = [
            'inventory_activity' => $inventory_activity,
            'shortage_total_count' => $shortage_total_count,
            'shortage_total_price' => $shortage_total_price,
        ];

        return view('admin.reports.reports_inventory_show', $data);        
    }
}
