<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
//use App\Models\OrderItem;

class OrderController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('can:view-orders');
    }

    public function index(){

    	$orders = Order::all();
    	$data = [
    		'orders' => $orders
    	];
    	return view('admin.orders.orders', $data);
    }

    public function order_items($order_id){

    	$order = Order::find($order_id);

    	$data = [
    		'order_items' => isset($order->items) ? $order->items : []
    	];
    	return view('admin.orders.order_items', $data);
    	
    }    
}
