<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateInnerDocumentRequest;
use App\Http\Requests\UpdateInnerDocumentRequest;
use App\Repositories\InnerDocumentRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class InnerDocumentController extends AppBaseController
{
    /** @var  InnerDocumentRepository */
    private $innerDocumentRepository;

    public function __construct(InnerDocumentRepository $innerDocumentRepo)
    {
        $this->middleware('auth');

        $this->middleware('can:view-inner-documents');

        $this->innerDocumentRepository = $innerDocumentRepo;
    }

    /**
     * Display a listing of the InnerDocument.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->innerDocumentRepository->pushCriteria(new RequestCriteria($request));
        $innerDocuments = $this->innerDocumentRepository->all();

        return view('admin.inner_documents.index')
            ->with('innerDocuments', $innerDocuments);
    }

    /**
     * Show the form for creating a new InnerDocument.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.inner_documents.create');
    }

    /**
     * Store a newly created InnerDocument in storage.
     *
     * @param CreateInnerDocumentRequest $request
     *
     * @return Response
     */
    public function store(CreateInnerDocumentRequest $request)
    {
        $input = $request->all();

        $innerDocument = $this->innerDocumentRepository->create($input);

        Flash::success('Inner Document saved successfully.');

        return redirect(route('innerDocuments.index'));
    }

    /**
     * Display the specified InnerDocument.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $innerDocument = $this->innerDocumentRepository->findWithoutFail($id);

        if (empty($innerDocument)) {
            Flash::error('Inner Document not found');

            return redirect(route('innerDocuments.index'));
        }

        return view('admin.inner_documents.show')->with('innerDocument', $innerDocument);
    }

    /**
     * Show the form for editing the specified InnerDocument.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $innerDocument = $this->innerDocumentRepository->findWithoutFail($id);

        if (empty($innerDocument)) {
            Flash::error('Inner Document not found');

            return redirect(route('innerDocuments.index'));
        }

        return view('admin.inner_documents.edit')->with('innerDocument', $innerDocument);
    }

    /**
     * Update the specified InnerDocument in storage.
     *
     * @param  int              $id
     * @param UpdateInnerDocumentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInnerDocumentRequest $request)
    {
        $innerDocument = $this->innerDocumentRepository->findWithoutFail($id);

        if (empty($innerDocument)) {
            Flash::error('Inner Document not found');

            return redirect(route('innerDocuments.index'));
        }

        $innerDocument = $this->innerDocumentRepository->update($request->all(), $id);

        Flash::success('Inner Document updated successfully.');

        return redirect(route('innerDocuments.index'));
    }

    /**
     * Remove the specified InnerDocument from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $innerDocument = $this->innerDocumentRepository->findWithoutFail($id);

        if (empty($innerDocument)) {
            Flash::error('Inner Document not found');

            return redirect(route('innerDocuments.index'));
        }

        $this->innerDocumentRepository->delete($id);

        Flash::success('Inner Document deleted successfully.');

        return redirect(route('innerDocuments.index'));
    }
}
