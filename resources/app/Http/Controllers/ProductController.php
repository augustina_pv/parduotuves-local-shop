<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Repositories\ProductRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

use App\Models\Product;
use App\Models\Category;
use App\Models\PvmGroup;
use App\Models\Measure;
use Storage;

//use App\Models\UserActivity;
//use Auth;
//use DB;
//use Illuminate\Http\File;


class ProductController extends AppBaseController
{
    /** @var  ProductRepository */
    private $productRepository;

    public function __construct(ProductRepository $productRepo)
    {
        $this->middleware('auth');
        
        $this->middleware('can:view-products');

        $this->productRepository = $productRepo;
    }

    /**
     * Display a listing of the Product.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        //var_dump( $this->authorize('view-products') );
            //return redirect(route('home'));


        $categories = Category::pluck('title', 'id');
        $measures = Measure::pluck('title', 'id');
        //$pvm_groups = PvmGroup::pluck('title', 'id')->toArray();

        $category_id = $request->category;
        $measure_id = $request->measure;
        //$pvm_group_id = $request->pvm_group;

        $products = Product::where(function($query) use($category_id, $measure_id){

            if($category_id)
                $query->whereHas('categories', function ($query) use($category_id) {
                    $query->where('id', $category_id);
                });

            if($measure_id)
                $query->where('measure_id', '=', $measure_id);   

        })->get(); 


        return view('admin.products.index', compact('products', 'measures', 'categories', 'category_id', 'measure_id'));
    }

    /**
     * Show the form for creating a new Product.
     *
     * @return Response
     */
    public function create()
    {
        //$categories = Category::all();
        //$cat_select = get_select_categories($categories, []);

        $categories = Category::where('parent_id', 0)->orderBy('parent_id', 'asc')->get();

        $pvm_groups = PvmGroup::pluck('title', 'id');
        $measures = Measure::pluck('title', 'id');


        $data = [
            //'categories' => $cat_select,
            'categories' => $categories,
            'pvm_groups' => $pvm_groups,
            'measures'  => $measures
        ];
        return view('admin.products.create', $data);
    }

    /**
     * Store a newly created Product in storage.
     *
     * @param CreateProductRequest $request
     *
     * @return Response
     */
    public function store(CreateProductRequest $request)
    {

        $product = new Product;

        if(isset($request->title))
            $product->title = $request->title;

        if(isset($request->sku))
            $product->sku = $request->sku;

        if(isset($request->barcode))
            $product->barcode = $request->barcode;

        if(isset($request->price))
            $product->price = $request->price;

        if(isset($request->price_purchase))
            $product->price_purchase = $request->price_purchase; 

        if(isset($request->count))
            $product->count = $request->count;

        if(isset($request->pvm_group_id))
            $product->pvm_group_id = $request->pvm_group_id;

        if(isset($request->measure_id))
            $product->measure_id = $request->measure_id;        

        if(isset($request->image)){

            $product->image = store_image_on_disk($request, 'image', 'products'); //return image path
        }

        $product->save();
        $product->categories()->attach($request->categories);
       

        Flash::success('Produktas išsaugotas sėkmingai.');


        //---- invetrory page - modal new product ------
        if($request['inventory'])
            return redirect(route('pajamavimas.index'));
        //----------------------------------------------


        return redirect(route('products.index'));
    }

    /**
     * Display the specified Product.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $product = $this->productRepository->findWithoutFail($id);      

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        $categories = Category::all();
        //$pvm_groups = PvmGroup::pluck('title', 'id')->toArray();
        //$measures = Measure::pluck('title', 'id')->toArray();
        //$cat_select = get_select_categories($categories, $product->categories); 


        $data = [
            'product' => $product,
            //'categories' => $cat_select,
            //'pvm_groups' => $pvm_groups,
            //'measures' => $measures,
        ];

        return view('admin.products.show', $data);
    }


    /**
     * Show the form for editing the specified Product.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $product = $this->productRepository->findWithoutFail($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        //$categories = Category::all();
        //$cat_select = get_select_categories($categories, $product->categories);

        $categories = Category::where('parent_id', 0)->orderBy('parent_id', 'asc')->get();
        $pvm_groups = PvmGroup::pluck('title', 'id');
        $measures = Measure::pluck('title', 'id');

        //$tests = Measure::pluck('title', 'id');

        $data = [
            'product' => $product,
            //'categories' => $cat_select,
            'categories' => $categories,
            'pvm_groups' => $pvm_groups,
            'measures'  => $measures,
            //'tests'      => $tests
        ];

        return view('admin.products.edit', $data);
    }

    /**
     * Update the specified Product in storage.
     *
     * @param  int              $id
     * @param UpdateProductRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductRequest $request)
    {


        //var_dump($request->count);
        //die;

        $product = Product::find($id);

        //abort_unless(Gate::allows('update', $product), 403);
        //$this->authorize('update', $product);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        
        if(isset($request->title))
            $product->title = $request->title;

        if(isset($request->sku))
            $product->sku = $request->sku;

        if(isset($request->barcode))
            $product->barcode = $request->barcode;

        if(isset($request->price))
            $product->price = $request->price;

        if(isset($request->price_purchase))
            $product->price_purchase = $request->price_purchase;        

        if(isset($request->count))
            $product->count = $request->count;

        if(isset($request->pvm_group_id))
            $product->pvm_group_id = $request->pvm_group_id;
        else
            $product->pvm_group_id = null;

        if(isset($request->measure_id))
            $product->measure_id = $request->measure_id;  
        else
            $product->measure_id = null;

        if(isset($request->image)){

            if( !empty($product->image) ){
                //DELETE FILE
                //$file_path = '2OPoYn7waSnE3yghOwrZCgY7cjKjDQuQza4HGpty.jpeg';
                //$file_path = $product->image;
                //Storage::delete($file_path);
                //Storage::disk('products')->delete($file_path);
                //var_dump($file_path);
                //die;
            }
            
            $product->image = store_image_on_disk($request, 'image', 'products'); //return image path
        }
        $product->update();
        $product->categories()->sync($request->categories);
               

        Flash::success('Produktas atnaujintas sėkmingai.');

        //return redirect(route('products.index'));
        return redirect()->back();
    }

    /**
     * Remove the specified Product from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $product = $this->productRepository->findWithoutFail($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        $this->productRepository->delete($id);

        //USER ACTIVITY
        add_user_activity($product, 'DELETED');

        Flash::success('Product deleted successfully.');

        return redirect(route('products.index'));
    }

    public function trash(){

        $products = Product::onlyTrashed()->get();

        /*
        foreach ($products as $product) {
            $activities = $product->user_activities->last();
            var_dump($activities->user_id);
            var_dump($activities->user->name);
        }
        */

        $data = [
            'products' => $products,
        ];
        return view('admin.trash.products.index', $data);
        
    }

    public function restore($id){

        $product = Product::withTrashed()->find($id);
        $title = $product->title;
        $product->restore();
        Flash::success('Produktas <strong>'.$title.'</strong> atkurtas sėkmingai.');

        //USER ACTIVITY
        add_user_activity($product, 'RESTORED');

        return redirect()->back();
    }

}
