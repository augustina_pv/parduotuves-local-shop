<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateMeasureRequest;
use App\Http\Requests\UpdateMeasureRequest;
use App\Repositories\MeasureRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class MeasureController extends AppBaseController
{
    /** @var  MeasureRepository */
    private $measureRepository;

    public function __construct(MeasureRepository $measureRepo)
    {
        $this->middleware('auth');

        $this->middleware('can:view-measures');

        $this->measureRepository = $measureRepo;
    }

    /**
     * Display a listing of the Measure.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->measureRepository->pushCriteria(new RequestCriteria($request));
        $measures = $this->measureRepository->all();

        return view('admin.measures.index')
            ->with('measures', $measures);
    }

    /**
     * Show the form for creating a new Measure.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.measures.create');
    }

    /**
     * Store a newly created Measure in storage.
     *
     * @param CreateMeasureRequest $request
     *
     * @return Response
     */
    public function store(CreateMeasureRequest $request)
    {
        $input = $request->all();

        $measure = $this->measureRepository->create($input);

        Flash::success('Measure saved successfully.');

        return redirect(route('measures.index'));
    }

    /**
     * Display the specified Measure.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $measure = $this->measureRepository->findWithoutFail($id);

        if (empty($measure)) {
            Flash::error('Measure not found');

            return redirect(route('measures.index'));
        }

        return view('admin.measures.show')->with('measure', $measure);
    }

    /**
     * Show the form for editing the specified Measure.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $measure = $this->measureRepository->findWithoutFail($id);

        if (empty($measure)) {
            Flash::error('Measure not found');

            return redirect(route('measures.index'));
        }

        return view('admin.measures.edit')->with('measure', $measure);
    }

    /**
     * Update the specified Measure in storage.
     *
     * @param  int              $id
     * @param UpdateMeasureRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMeasureRequest $request)
    {
        $measure = $this->measureRepository->findWithoutFail($id);

        if (empty($measure)) {
            Flash::error('Measure not found');

            return redirect(route('measures.index'));
        }

        $measure = $this->measureRepository->update($request->all(), $id);

        Flash::success('Measure updated successfully.');

        return redirect(route('measures.index'));
    }

    /**
     * Remove the specified Measure from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $measure = $this->measureRepository->findWithoutFail($id);

        if (empty($measure)) {
            Flash::error('Measure not found');

            return redirect(route('measures.index'));
        }

        $this->measureRepository->delete($id);

        Flash::success('Measure deleted successfully.');

        return redirect(route('measures.index'));
    }
}
