<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\UserRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

use App\Models\User;
use App\Models\Role;
use App\Models\Permission;
use Storage;

class UserController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->middleware('auth');

        $this->middleware('can:view-users');

        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of the User.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->userRepository->pushCriteria(new RequestCriteria($request));
        $users = $this->userRepository->all();
        //$permissions = Permission::all();

        $data = [
            'users' => $users,
            //'permissions' => $permissions
        ];

        return view('admin.users.index', $data);
    }

    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create()
    {
        $roles = Role::pluck('name', 'id')->toArray();
        $permissions = Permission::all();

        return view('admin.users.create', compact('roles', 'permissions'));
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {
        $input = $request->all();

        $input['password'] = bcrypt($input['password']);
        $user = $this->userRepository->create($input);

        $user->permissions()->attach($request->permissions);

        Flash::success('User saved successfully.');

        return redirect(route('users.index'));
    }

    /**
     * Display the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }


        //foreach ($user->permissions as $permission) {
        //    var_dump($permission->name);
        //}
        //die;

        return view('admin.users.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        $roles = Role::pluck('name', 'id')->toArray();
        $permissions = Permission::all();

        $data = [
            'user' => $user,
            'roles' => $roles,
            'permissions' => $permissions
        ];

        return view('admin.users.edit', $data);
    }

    /**
     * Update the specified User in storage.
     *
     * @param  int              $id
     * @param UpdateUserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserRequest $request)
    {
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        $user = User::find($id);
        if ($request->name)
            $user->name = $request->name;
        if ($request->lastname)
            $user->lastname = $request->lastname;
        if ($request->email)
            $user->email = $request->email;
        if ($request->role_id)
            $user->role_id = $request->role_id;

        if(isset($request->image)){

            $filename = $request->image->getClientOriginalName();

            //$user->image = store_image_on_disk($request, 'image', 'public'); //return image path

            //dir = public/users
            $filename_store = Storage::disk('users')->putFileAs('', $request->file('image'), $filename);
            $user->image = 'users/'.$filename_store;
        }        


        if ($request->password){
            
            $user->password = bcrypt($request->password);
        }

        $user->update();

        $user->permissions()->sync($request->permissions);


        //$user = $this->userRepository->update($request->all(), $id);

        Flash::success('User updated successfully.');

        return redirect(route('users.index'));
    }

    /**
     * Remove the specified User from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        $this->userRepository->delete($id);

        Flash::success('User deleted successfully.');

        return redirect(route('users.index'));
    }
}
