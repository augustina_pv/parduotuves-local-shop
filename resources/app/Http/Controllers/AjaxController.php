<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SupplierDocument;
use App\Models\Transaction;
use App\Models\Product;
use App\Models\Order;
use App\Models\InventoryActivity;
use App\Models\InventoryActivitiesItem;
use Auth;
use Illuminate\Support\Carbon;

define('PIRKIMAS', 1);

class AjaxController extends Controller
{
    
    //---------------- AJAX MAIN --------------------
    function index(Request $request){

    	$action = $request['action'];
        $func = 'ajax_'.$action;
        
        if (!method_exists($this, $func))
            echo json_encode( ['error'=>'bad_action'] );
        else
            call_user_func( array($this, $func), $request );
    }

    //$.ajax = action: 'test' 
    //$.ajax({ data{ action: 'test' } })
    public function ajax_test(){ 
        $data = [1, 2];
        echo json_encode($data);
    }

    //-------------------------------------------------------





    //----------------------------------- PAJAMAVIMAS ------------------------------------------------

    private function insert_supplier_document($supplier_id, $document_nr){
        $supplier_document = new SupplierDocument;
        $supplier_document->supplier_id = $supplier_id;
        $supplier_document->nr = $document_nr;
        $supplier_document->date = now();
        $res = $supplier_document->save();

        return $supplier_document->id;
    }

    //---------------- START PROCESS (pajamavimas) -----------------------------------
    public function ajax_start_process_supplier($request){

        $supplier_id = $request['supplier_id'];
        $document_nr = $request['document_nr'];

        $status = 1; //OK
        $message = 'Operacija Vykdoma';


        //------- INSERT DB & SESSION supplier_document_id -------
        $supplier_document_id = session('supplier_document_id');
        $supplier_document_id_found = [];


        $db_doc_nr = SupplierDocument::where('nr', $document_nr)->first();
        $unique_doc_nr = isset($db_doc_nr) ? false : true;

        
        if( isset($supplier_document_id) ){
            $supplier_document_id_found = SupplierDocument::find($supplier_document_id);
            //$supplier_document_id_found = true;
        }

        if( empty($supplier_document_id_found) && $unique_doc_nr ){
            $supplier_document_id = $this->insert_supplier_document($supplier_id, $document_nr);
            session(['supplier_document_id' => $supplier_document_id]);
        }
        
        
        //--------------------------------------------------------   

        $data = [
            'status' => $status,
            'unique_doc_nr' => $unique_doc_nr,
            'supplier_document_id' => $supplier_document_id,
            'message' => $message
        ];
        echo json_encode($data);
    }


    //------------- FINISH PROCESS ----------------------------
    public function ajax_finish_process_supplier($request){
    	$supplier_document_id = session('supplier_document_id');
        if( isset($supplier_document_id) ){
            session(['supplier_document_id' => null]);

            //update product counts
            $this->update_product_counts($supplier_document_id);
        }


        $data = [
            'status' => 1,
            'supplier_document_id' => session('supplier_document_id'),
            'message' => 'Operacija Užbaigta',
        ];
        echo json_encode($data);
    }

    private function update_product_counts($supplier_document_id){
        $supplier_document = SupplierDocument::find($supplier_document_id);
        foreach ($supplier_document->transactions as $transaction) {

            $product_id = $transaction->product->id;
            $add_product_count = $transaction->count;

            $product = Product::find($product_id);
            $product->count += $add_product_count;
            $product->save();
        }        
    }


    //---------------- INSERT PRODUCT ------------------------
    public function ajax_insert_inventory_product($request){

        $status = 0; //nepradeta
        $message = '';

        $supplier_document_id = session('supplier_document_id');

        $document_id = $supplier_document_id;
        $type_id = PIRKIMAS; //TIPAS = PREKE GAUTA (pirkimas)
        $product_id = $request['product_id'];
        $product_title = $request['product_title'];
        $count = $request['count'];
        $price_purchase = $request['price_purchase'];   
        $price_sale = $request['price_sale'];      

        $transaction_id = null;
        $product_html = null;
 


        if( !isset($supplier_document_id) ){
            $status = -1; //ended session
            $message = '
                <ul class="alert alert-danger" style="list-style-type: none">
                    <li>Operacija nepradėta vykdyti arba baigėsi sesija</li>
                </ul>
            ';
        }
        else{
            $transaction_id = $this->insert_transaction($document_id, $type_id, $product_id, $count, $price_purchase, $price_sale);
            if($transaction_id){
                $product = Product::find($product_id);
                $product->price = $price_sale;
                $product->price_purchase = $price_purchase;
                //$product->count += $count;
                $product->save();
            }

            $status = 1; //OK
            $message = 'Produktas įdėtas';

            $supplier_document = SupplierDocument::find($supplier_document_id);
            //->orderBy('id', 'DESC')

            $data = [
                'supplier_document' => $supplier_document
            ];
            $view = view('admin.goods_receiving.products', $data);
            //
            $product_html = $view->render();
        }

        
        $data = [
            'status' => $status,
            'supplier_document_id' => session('supplier_document_id'),
            'transaction_id' => $transaction_id,
            'message' => $message,
            'html' => $product_html,
        ];
        echo json_encode($data);  
           
    }


    //Pirkimas
    private function insert_transaction($document_id, $type_id, $product_id, $count, $price_purchase, $price_sale){

        $user_id = Auth::id();
        $supplier_document = SupplierDocument::find($document_id);

        if($supplier_document){
            $transaction = new Transaction;
            //$transaction->document_id = $document_id;
            //$transaction->document_type = 'App\Models\SupplierDocument'; //pirkimas
            $transaction->type_id = $type_id;
            $transaction->product_id = $product_id;
            $transaction->count = $count;
            $transaction->price_purchase = $price_purchase;
            $transaction->price = $price_sale;
            $transaction->user_id = $user_id;
            //$transaction->save();

            $supplier_document->transactions()->save($transaction);

            return $transaction->id;
        }

        return false;
    }    


    //------------ GET PRODUCT PRICE -------------------
    public function ajax_get_product_price($request){

        $status = 0;
        $price_purchase = null;
        $price = null;

        if ($request['prod_id']){
            $id = $request['prod_id'];
            $product = Product::find($id);
            $price_purchase = $product->price_purchase;
            $price = $product->price;
            $status = 1; //OK
        }

        $data = [
            'status' => $status,
            'price_purchase' => $price_purchase,
            'price' => $price,
        ];
        echo json_encode($data);
    }    


    //--------------- NULL ORDER STATUS -----------------------
    public function ajax_null_order_status($request){

        $order_id = $request->order_id;

        $order = Order::find($order_id);
        $order->status = 0;
        $updated = $order->update();

        $order = Order::find($order_id);

        $data = [
            'updated' => $updated,
            'status' => $order->status
        ];

        echo json_encode($data);        
    }
    //---------------------- END PAJAMAVIMAS --------------------------------------------------





    //------------------------------ INVENTORY -----------------------------------

    //---------------- START PROCESS -----------------------------------
    public function ajax_start_process_inventory_activity($request){


        $inventory_activity = new InventoryActivity;
        $inventory_activity->title = isset($request->title) ? $request->title : null;
        $inventory_activity->user_id = Auth::id();
        $saved = $inventory_activity->save();
        $inventory_activity_id = $saved ? $inventory_activity->id : 0;

        if($inventory_activity_id > 0)
            session(['inventory_activity_id' => $inventory_activity_id]);
        
        //--------------------------------------------------------   

        $data = [
            'saved' => $saved,
            'activity_id' => $inventory_activity_id,
            'title' => $request->title,
        ];
        echo json_encode($data);
    }    


    //---------------- FINISH PROCESS -----------------------------------
    public function ajax_finish_process_inventory_activity($request){

        $saved = 0;

        $activity_id = $request->activity_id;
        $products = $request->products;


        foreach ($products as $product){
            $prod_id = isset($product['prod_id']) ? intval($product['prod_id']) : 0;
            $price = isset($product['price']) ? floatval($product['price']) : null;
            $count = isset($product['count']) ? intval($product['count']) : 0;
            $real_count = isset($product['real_count']) ? intval($product['real_count']) : 0;

            
            $inventory_activity_item = new InventoryActivitiesItem;
            $inventory_activity_item->inventory_activity_id = intval($activity_id);
            $inventory_activity_item->product_id = $prod_id;
            $inventory_activity_item->price = $price;
            $inventory_activity_item->count = $count;
            $inventory_activity_item->count_real = $real_count;
            $saved = $inventory_activity_item->save();
        
        }

        if($saved){
            session(['inventory_activity_id' => null]);
            $incentory_activity = InventoryActivity::find($activity_id);
            $incentory_activity->status = 1; //compleated
            $incentory_activity->update();
        }
        
        //--------------------------------------------------------   

        $data = [
            'saved' => $saved,
            'activity_id' => $activity_id,
            'products' => $products
        ];

        echo json_encode($data);
    }  


}
