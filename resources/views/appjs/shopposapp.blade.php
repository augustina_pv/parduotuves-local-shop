@extends('appjs.shop_layout')

@section('top_scripts')
	<script src="{{ asset('js/shopPosApp.js') }}"></script>
@endsection

@section('bottom_scripts')
	<script>
		$( function() {
			$( "#summary-report-from" ).datepicker({ dateFormat: 'yy-mm-dd' });
			$( "#summary-report-to" ).datepicker({ dateFormat: 'yy-mm-dd' });
			$( "#detailed-report-from" ).datepicker({ dateFormat: 'yy-mm-dd' });
			$( "#detailed-report-to" ).datepicker({ dateFormat: 'yy-mm-dd' });
			
		});
	</script>
@endsection

@section('content')

<div id="shoppos" v-cloak class="content">
	<input id="storage-url" type="hidden" value="{{ url('/')  }}{{ Storage::url('') }}">
	<input id="logged-user-id" type="hidden" value="{{ Auth::id() }}">
	<input id="pvm-rate" type="hidden" value="{{ env('PVM') }}">

	<div class="front-right-side full-width">
		<div class="container-fluid">

			<!-- TOOLBAR -->
			<div class="row toolbar-row">
				<a href="{{ route('shop-app-js') }}">
					<div class="toolbar-icon pull-right">
						<i class="fa fa-home"></i>
					</div>
				</a>				
			</div>

			<div class="space-lg hidden"></div>

			<shop-pos  
				v-bind:pos_insert_total="$store.getters['posActions/insert_total']"
				v-bind:pos_withdraw_total="$store.getters['posActions/withdraw_total']"
				v-bind:pos_refund_total="$store.getters['posActions/refund_total']"
				v-bind:pos_cash_total="$store.getters['posActions/cash_total']"
				v-bind:pos_card_total="$store.getters['posActions/card_total']"
				v-bind:pos_orders_total="$store.getters['posActions/orders_total']"
				v-bind:pos_money_total="$store.getters['posActions/money_total']"
				v-bind:pos_money_in_pos="$store.getters['posActions/money_in_pos']"
				v-bind:action_message="action_message"
				v-bind:action_message_active="action_message_active"
				v-on:insert_pos_action_click="on_shop_pos_insert_action_click"
			></shop-pos>

		</div>
	</div>


</div>	


@endsection	