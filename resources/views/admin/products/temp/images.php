<div class="form-group col-sm-12" style="display: none">
    <div class="imageupload panel panel-default">
        <div class="panel-heading clearfix">
            <h3 class="panel-title pull-left">Upload Image</h3>
        </div>
        <div class="file-tab panel-body">
            <label class="btn btn-default btn-file">
                <span>Browse</span>
                <!-- The file is stored here. -->
                <input type="file" name="image-file">
            </label>
            <button type="button" class="btn btn-default">Remove</button>
        </div>
        <div class="url-tab panel-body">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Image URL">
                <div class="input-group-btn">
                    <button type="button" class="btn btn-default">Submit</button>
                </div>
            </div>
            <button type="button" class="btn btn-default">Remove</button>
            <!-- The URL is stored here. -->
            <input type="hidden" name="image-url">
        </div>
    </div>
</div>