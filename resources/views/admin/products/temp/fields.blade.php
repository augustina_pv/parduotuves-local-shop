<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Sku Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sku', 'Sku:') !!}
    {!! Form::text('sku', null, ['class' => 'form-control']) !!}
</div>

<!-- Barcode Field -->
<div class="form-group col-sm-6">
    {!! Form::label('barcode', 'Barcode:') !!}
    {!! Form::text('barcode', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', 'Price:') !!}
    {!! Form::text('price', null, ['class' => 'form-control']) !!}
</div>

<!-- Count Field -->
<div class="form-group col-sm-6">
    {!! Form::label('count', 'Count:') !!}
    {!! Form::text('count', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', 'Image:') !!}
    @if( !empty($product) )
        <div class="product-image">
            <img class="small-image" src="{{ Storage::url($product->image) }}">
        </div>
    @endif
    {!! Form::file('image', null, ['class' => 'form-control']) !!}
</div>


<div class="clearfix"></div>


<!-- PVM Groups Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pvm_group_id', 'Pvm Grupės:') !!}
    {{ Form::select('pvm_group_id', $pvm_groups, null, ['class'=>'form-control', 'placeholder' => '']) }}
</div>

<!-- Measures Field -->
<div class="form-group col-sm-6">
    {!! Form::label('measure_id', 'Išmatavimai:') !!}
    {{ Form::select('measure_id', $measures, null, ['class'=>'form-control', 'placeholder' => '']) }}
</div>


<div class="clearfix"></div>


<!-- Categories Field -->
<div class="form-group col-sm-12">
    {!! Form::label('categories', 'Categories:') !!}
    @foreach($categories as $category)
        <div class="category">
            {{ $category['title'] }} {{ Form::checkbox('categories[]', $category['id'], $category['selected'] ) }}
        </div>
    @endforeach
    
</div>




<div class="form-group col-sm-12" style="display: none">
    <div class="imageupload panel panel-default">
        <div class="panel-heading clearfix">
            <h3 class="panel-title pull-left">Upload Image</h3>
        </div>
        <div class="file-tab panel-body">
            <label class="btn btn-default btn-file">
                <span>Browse</span>
                <!-- The file is stored here. -->
                <input type="file" name="image-file">
            </label>
            <button type="button" class="btn btn-default">Remove</button>
        </div>
        <div class="url-tab panel-body">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Image URL">
                <div class="input-group-btn">
                    <button type="button" class="btn btn-default">Submit</button>
                </div>
            </div>
            <button type="button" class="btn btn-default">Remove</button>
            <!-- The URL is stored here. -->
            <input type="hidden" name="image-url">
        </div>
    </div>
</div>



<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('products.index') !!}" class="btn btn-default">Cancel</a>
</div>
