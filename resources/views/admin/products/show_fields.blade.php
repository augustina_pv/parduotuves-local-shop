
<div class="col-sm-6">
 
    <!-- Id Field -->
    <div class="form-group">
        {!! Form::label('id', 'Id:') !!}
        <p>{!! $product->id !!}</p>
    </div>

    <!-- Title Field -->
    <div class="form-group">
        {!! Form::label('title', Lang::get('base.title').':') !!}
        <p>{!! $product->title !!}</p>
    </div>

    <!-- Sku Field -->
    <div class="form-group">
        {!! Form::label('sku', Lang::get('base.product_code').':') !!}
        <p>{!! $product->sku !!}</p>
    </div>

    <!-- Barcode Field -->
    <div class="form-group">
        {!! Form::label('barcode', Lang::get('base.barcode').':') !!}
        <p>{!! $product->barcode !!}</p>
    </div>

    <!-- Price Field -->
    <div class="form-group">
        {!! Form::label('price', Lang::get('base.price').':') !!}
        <p>{!! $product->price !!}</p>
    </div>

    <!-- Price Field -->
    <div class="form-group">
        {!! Form::label('price_purchase', Lang::get('base.price_purchase').':') !!}
        <p>{!! $product->price_purchase !!}</p>
    </div>    

    <!-- Count Field -->
    <div class="form-group">
        {!! Form::label('count', Lang::get('base.count').':') !!}
        <p>{!! $product->count !!}</p>
    </div>

    <!-- Categories Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('categories', Lang::get('base.categories').':') !!}

        @foreach($product->categories as $category)
            <div class="category">
                {{ $category['title'] }}
            </div>
        @endforeach
        
    </div>


    <!-- PVM Groups Field -->
    <div class="form-group">
        {!! Form::label('pvm_group_id', Lang::get('base.pvm_group').':') !!}
        {{ $product->pvm_group ? $product->pvm_group->title : '' }}
    </div>

    <!-- Measures Field -->
    <div class="form-group">
        {!! Form::label('measure_id', Lang::get('base.measure').':') !!}
        {{ $product->measure ? $product->measure->title : '' }}
    </div>

    <!-- Created At Field -->
    <div class="form-group">
        {!! Form::label('created_at', Lang::get('base.created_at').':') !!}
        <p>{!! $product->created_at !!}</p>
    </div>

    <!-- Updated At Field -->
    <div class="form-group">
        {!! Form::label('updated_at', Lang::get('base.updated_at').':') !!}
        <p>{!! $product->updated_at !!}</p>
    </div>    

</div>


<div class="col-sm-6">
    <!-- Image Field -->
    <div class="form-group">
        {!! Form::label('image', Lang::get('base.image').':') !!}
        <div class="image">
            <img class="big-image" src="{{ Storage::url($product->image) }}">
        </div>    

    </div>
</div>

