<div class="col-sm-6">
    <!-- Title Field -->
    <div class="form-group">
        {!! Form::label('title', Lang::get('base.title').':') !!}
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Sku Field -->
    <div class="form-group">
        {!! Form::label('sku', Lang::get('base.product_code').':') !!}
        {!! Form::text('sku', null, ['class' => 'form-control']) !!}
    </div>    

    <!-- Barcode Field -->
    <div class="form-group">
        {!! Form::label('barcode', Lang::get('base.barcode').':') !!}
        {!! Form::text('barcode', null, ['class' => 'form-control']) !!}
    </div>   

    <!-- Price Field -->
    <div class="form-group">
        {!! Form::label('price', Lang::get('base.price_no_pvm').':') !!}
        {!! Form::text('price', null, ['class' => 'form-control']) !!}
    </div>   

    <!-- Price Purchase Field -->
    <div class="form-group">
        {!! Form::label('price_purchase', Lang::get('base.price_purchase').':') !!}
        {!! Form::text('price_purchase', null, ['class' => 'form-control']) !!}
    </div>      

    <!-- Count Field -->
    <div class="form-group">
        {!! Form::label('count', Lang::get('base.count').':') !!}
        {!! Form::text('count', null, ['class' => 'form-control']) !!}
    </div>      

    <!-- Image Field -->
    <div class="form-group">
        {!! Form::label('image', Lang::get('base.image').':') !!}
        @if( !empty($product->image) )
            
            <div class="product-image">
                <img class="small-image" src="@if($product->image) {{ asset('storage') }}/{{ $product->image }} @endif">
            </div>
        @endif
        {!! Form::file('image', null, ['class' => 'form-control']) !!}
    </div>   


    <!-- PVM Groups Field -->
    <div class="form-group">
        {!! Form::label('pvm_group_id', Lang::get('base.pvm_group').':') !!}
        {{ Form::select('pvm_group_id', $pvm_groups, null, ['class'=>'form-control', 'placeholder' => '']) }}
    </div>

    <!-- Measures Field -->
    <div class="form-group">
        {!! Form::label('measure_id', Lang::get('base.measure').':') !!}
        {{ Form::select('measure_id', $measures, null, ['class'=>'form-control', 'placeholder' => '']) }}
    </div>
</div>

<div class="col-sm-6">
    <!-- Categories Field -->
    <div class="form-group">
        <h3>@lang('base.categories'):</h3>
        @include('admin.products.categories_and_childs', ['categories' => $categories])
    </div>
</div>




