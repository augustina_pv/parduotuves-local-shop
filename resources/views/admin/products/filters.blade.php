<!-- Categories Field -->
<div class="form-group col-sm-2">
    {!! Form::label('category_id', 'Kategorijos:') !!}
    {{ Form::select(
        'category_id',
        $categories,
        $category_id,
        ['class'=>'form-control', 'placeholder' => '', 'placeholder'=>'Visos']) 
    }}
</div> 

<!-- Measures Field -->
<div class="form-group col-sm-2">
    {!! Form::label('measure_id', 'Išmatavimai:') !!}
    {{ Form::select(
        'measure_id',
        $measures,
        $measure_id,
        ['class'=>'form-control', 'placeholder' => '', 'placeholder'=>'Visi']) 
    }}
</div>  

<!-- Filtruoti Field -->
<div class="form-group col-sm-2">
    {{ Form::button(
        'Filtruoti',
        ['id'=>'filter-product', 'class'=>'btn btn-primary buttons-group']) 
    }}
</div> 

<!-- Visi Produktai Field -->
<div class="form-group col-sm-2">
    <a class="btn btn-primary buttons-group" href="{{ route('products.index') }}">Visi Produktai</a>
</div>




