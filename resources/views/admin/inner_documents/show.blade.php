@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Inner Document
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('admin.inner_documents.show_fields')
                    <a href="{!! route('innerDocuments.index') !!}" class="btn btn-default">@lang('base.back')</a>
                </div>
            </div>
        </div>
    </div>
@endsection
