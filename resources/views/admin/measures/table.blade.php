<table class="table table-responsive" id="measures-table">
    <thead>
        <tr>
            <th>@lang('base.title')</th>
            <th colspan="3"></th>
        </tr>
    </thead>
    <tbody>
    @foreach($measures as $measure)
        <tr>
            <td>{!! $measure->title !!}</td>
            <td>
                {!! Form::open(['route' => ['measures.destroy', $measure->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('measures.show', [$measure->id]) !!}" class='btn btn-default'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('measures.edit', [$measure->id]) !!}" class='btn btn-default'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>