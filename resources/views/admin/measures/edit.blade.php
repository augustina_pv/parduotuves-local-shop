@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            @lang('layouts/menu.measure')
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($measure, ['route' => ['measures.update', $measure->id], 'method' => 'patch']) !!}

                        @include('admin.measures.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection