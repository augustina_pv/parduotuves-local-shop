<table class="table table-responsive" id="trashes-table">
    <thead>
        <tr>
            <th>Title</th>
            <th>User</th>
            <th>Deleted At</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($products as $product)
        <tr>
            <td>{!! $product['title'] !!}</td>
            <td>
                @if($product->user_activities->last()->user)
                    {!! $product->user_activities->last()->user->name !!}
                @endif
            </td>
            <td>{!! $product['deleted_at'] !!}</td>
            <td>

                {!! Form::model($product, ['route' => ['trash.products.restore', $product->id], 'method' => 'patch']) !!}
                    <div class='btn-group'>
                        
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-success', 'onclick' => "return confirm('Atnaujinti?')"]) !!} 
                        
                    </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>