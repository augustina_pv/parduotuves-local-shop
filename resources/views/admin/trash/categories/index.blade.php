@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Trash Categories</h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('admin.trash.categories.table')

                    <div class="clearfix"></div>
                    <a href="{{ route('trash.index') }}" class="btn btn-default">@lang('base.back')</a>
            </div>
        </div>
    </div>
@endsection

