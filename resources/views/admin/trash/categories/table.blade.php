<table class="table table-responsive" id="trashes-table">
    <thead>
        <tr>
            <th>ID</th>
            <th>@lang('base.title')</th>
            <th>@lang('base.childs')</th>
            <th>@lang('base.user')</th>
            <th>@lang('base.deleted_at')</th>
            <th colspan="3"></th>
        </tr>
    </thead>
    <tbody>
    @foreach($categories as $category)
        <tr>
            <td>{!! $category['id'] !!}</td>
            <td>{!! $category['title'] !!}</td>
            <td>
                {{-- {!! $category->childs->count() !!} --}}
                
                @foreach($category->childs as $child)
                    <span class="categories-inline">{{ $child->title }}</span>
                @endforeach
            </td>
            <td>
                @if($category->user_activities->last()->user)
                    {!! $category->user_activities->last()->user->name !!}
                @endif
            </td>            
            <td>{!! $category['deleted_at'] !!}</td>
            <td>

                {!! Form::model($category, ['route' => ['trash.categories.restore', $category->id], 'method' => 'patch']) !!}
                    <input name="model" type="hidden" value="category">
                    <div class='btn-group'>
                        
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-success', 'onclick' => "return confirm('Atnaujinti?')"]) !!} 
                        
                    </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>