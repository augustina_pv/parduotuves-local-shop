<table class="table table-responsive" id="trashes-table">
    <thead>
        <tr>
            <th>@lang('base.type')</th>
            <th>@lang('base.count')</th>
            <th colspan="3"></th>
        </tr>
    </thead>
    <tbody>
    
        <tr>
            <td>@lang('layouts/menu.categories')</td>
            <td>{{ $categories_count }}</td>
            <td>
                <div class='btn-group'>
                    <a href="{!! route('trash.categories') !!}" class='btn btn-default'><i class="glyphicon glyphicon-eye-open"></i></a>
                </div>
            </td>
        </tr>

        <tr>
            <td>@lang('layouts/menu.products')</td>
            <td>{{ $products_count }}</td>
            <td>
                <div class='btn-group'>
                    <a href="{!! route('trash.products') !!}" class='btn btn-default'><i class="glyphicon glyphicon-eye-open"></i></a>
                </div>
            </td>
        </tr>
    
    </tbody>
</table>