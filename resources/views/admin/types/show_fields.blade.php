<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $type->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', Lang::get('base.name').':') !!}
    <p>{!! $type->name !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', Lang::get('base.created_at').':') !!}
    <p>{!! $type->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', Lang::get('base.updated_at').':') !!}
    <p>{!! $type->updated_at !!}</p>
</div>

