@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            @lang('layouts/menu.types')
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('admin.types.show_fields')
                    <a href="{!! route('types.index') !!}" class="btn btn-default">@lang('base.back')</a>
                </div>
            </div>
        </div>
    </div>
@endsection
