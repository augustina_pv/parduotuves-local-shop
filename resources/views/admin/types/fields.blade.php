<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', Lang::get('base.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(Lang::get('base.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('types.index') !!}" class="btn btn-default">@lang('base.cancel')</a>
</div>
