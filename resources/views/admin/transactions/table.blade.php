<table class="table table-responsive" id="transactions-table">
    <thead>
        <tr>
            <th>@lang('base.product_id')</th>
            <th>@lang('base.document_id')</th>
            <th>@lang('base.type_id')</th>
            <th>@lang('base.user_id')</th>
            <th>@lang('base.count')</th>
            <th>@lang('base.price')</th>
            <th>@lang('base.price_purchase')</th>
            <th colspan="3"></th>
        </tr>
    </thead>
    <tbody>
    @foreach($transactions as $transaction)
        <tr>
            <td>
                {!! $transaction->product_id !!} 
                @if($transaction->getProductTitle())
                    ({!! $transaction->getProductTitle() !!})
                @else
                    (<span class="deleted-product">Nerasta DB</span>)
                @endif
            </td>
            <td>{!! $transaction->document_id !!}</td>
            <td>{!! $transaction->type_id !!} ({!! $transaction->type->name !!})</td>
            <td>{!! $transaction->user_id !!}</td>
            <td>{!! $transaction->count !!}</td>
            <td>{!! $transaction->price !!}</td>
            <td>{!! $transaction->price_purchase !!}</td>
            <td>
                {!! Form::open(['route' => ['transactions.destroy', $transaction->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('transactions.show', [$transaction->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('transactions.edit', [$transaction->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button(
                        '<i class="glyphicon glyphicon-trash"></i>', 
                        ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm( 'Are your sure?' )"])
                    !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>