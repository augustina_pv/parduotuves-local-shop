<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $transaction->id !!}</p>
</div>

<!-- Product Id Field -->
<div class="form-group">
    {!! Form::label('product_id', Lang::get('base.product_id').':') !!}
    <p>{!! $transaction->product_id !!}</p>
</div>

<!-- Document Id Field -->
<div class="form-group">
    {!! Form::label('document_id', Lang::get('base.document_id').':') !!}
    <p>{!! $transaction->document_id !!}</p>
</div>

<!-- Type Id Field -->
<div class="form-group">
    {!! Form::label('type_id', Lang::get('base.type_id').':') !!}
    <p>{!! $transaction->type_id !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', Lang::get('base.user_id').':') !!}
    <p>{!! $transaction->user_id !!}</p>
</div>

<!-- Count Field -->
<div class="form-group">
    {!! Form::label('count', Lang::get('base.count').':') !!}
    <p>{!! $transaction->count !!}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', Lang::get('base.price').':') !!}
    <p>{!! $transaction->price !!}</p>
</div>

<!-- Original Price Field -->
<div class="form-group">
    {!! Form::label('original_price', Lang::get('base.price_purchase').':') !!}
    <p>{!! $transaction->price_purchase !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', Lang::get('base.created_at').':') !!}
    <p>{!! $transaction->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', Lang::get('base.updated_at').':') !!}
    <p>{!! $transaction->updated_at !!}</p>
</div>

