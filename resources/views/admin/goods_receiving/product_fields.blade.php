<!-- Products Groups Field -->
<div class="col-sm-3">

	<div class="form-group">
	    {!! Form::label('product_id', Lang::get('base.product').':') !!}
	    <div class="products-select2">
	    	{{ Form::select('product_id', $products, 0, ['class'=>'form-control js-products', 'placeholder' => '--- Pasirinkite ---']) }}
	    </div>
	</div>	
</div>

<div class="col-sm-2">
	<div class="form-group">
	    {!! Form::label('count', Lang::get('base.count').':') !!}
	    {!! Form::text('count', null, ['class' => 'form-control', 'id' => 'count']) !!}
	</div>	
</div>

<div class="col-sm-2">
	<div class="form-group">
	    {!! Form::label('product_price_purchace', Lang::get('base.price_purchase').':') !!}
	    {!! Form::text('price_purchase', null, ['class' => 'form-control', 'id' => 'product_price_purchace']) !!}
	</div>								
</div>

<div class="col-sm-2">
	<div class="form-group">
	    {!! Form::label('product_price_sale', Lang::get('base.price_sale').':') !!}
	    {!! Form::text('price_sale', null, ['class' => 'form-control', 'id' => 'product_price_sale']) !!}
	</div>								
</div>

<div class="col-sm-3">
	<div class="buttons-group pull-left">
	    {!! Form::button('Įrašyti', ['class' => 'btn btn-success', 'id' => 'insert_inventory_product']) !!}
	</div>	
	<div class="buttons-group pull-right">
	    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-product-new">
            Naujas Produktas
        </button>
	</div>	

</div>

