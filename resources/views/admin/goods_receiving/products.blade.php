
	<table class="table table-responsive" id="products-table">
	    <thead>
	        <tr>
	            <th>#</th>
	            <th>@lang('base.title')</th>
	            <th>@lang('base.count')</th>
	            <th>@lang('base.price_purchase')</th>
	            <th>@lang('base.price_sale')</th>
	            <th colspan="3"></th>
	        </tr>
	    </thead>
	    <tbody>

	    @if($supplier_document)	
	    @foreach($supplier_document->transactions->sortByDesc('id') as $transaction)
	        <tr>
	        	<td>{!! $transaction->id !!}</td>
	            <td>
	            	@if($transaction->product)	
	            		{!! $transaction->product->title !!}
	            	@endif
	            </td>
	            <td>{!! $transaction->count !!}</td>
	            <td>{!! $transaction->price_purchase !!}</td>
	            <td>{!! $transaction->price !!}</td>

	            <td>
	                {!! Form::open(['route' => ['pajamavimas.destroy', $transaction->id], 'method' => 'delete']) !!}
	                <div class='btn-group'>

	                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Ar ištrinti šį produktą?')"]) !!}
	                </div>
	                {!! Form::close() !!}
	            </td>
	        </tr>
	    @endforeach
	    @endif

	    </tbody>
	</table>	
</div>




