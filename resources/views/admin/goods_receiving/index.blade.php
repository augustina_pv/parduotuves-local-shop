@extends('layouts.app')

@section('css')
@endsection

@section('scripts')

	<script type="text/javascript" src="{{ asset('js/inventory_scripts.js') }}"></script>


	<script>
		$(document).ready(function() {
		    
		    $('.js-products').select2();


		    /*
			$(document).on('click', '#test', function(){
				$(".js-products").select2("val", "0"); //set the value
			});			    
			*/
		});
	</script>

    <script>
        $("input[name='count']").TouchSpin({
        });
    </script>	

@endsection


@section('content')
    <section class="content-header">
        <h1 class="">@lang('layouts/menu.goods_receiving')</h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')
		<div class="row">
			<div class="col-sm-12">
				<div id="product_insert_message"></div>
			</div>
		</div>


        <div class="clearfix"></div>

        <div class="box box-primary">

            <div class="box-body">

            	<div class="row">
					@include('admin.goods_receiving.supplier_fields')
				</div>	


                <div id="products_body" class="panel panel-default @if($document_processing) show @endif">


                	<!-- Irasyti Produkta -->
                	<div class="panel-heading">
						<div class="row">
							@include('admin.goods_receiving.product_fields')
						</div>	
					</div>	
					

					<!-- Produktai -->
					<div class="panel-body">
						<div class="row">
							<div class="col-sm-12">

								<div id="inventory-products">
									@include('admin.goods_receiving.products')
								</div>

							</div>	
						</div>
					</div>

            	</div>


            </div>	

        </div>
    </div>

    <!-- Naujas Produktas -->
    <div class="modal fade" id="modal-product-new">
    	<div class="modal-dialog">
    		<div class="modal-content">
    			<div class="modal-header">
    				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    				<h4 class="modal-title">Naujas Produktas</h4>
    			</div>

    			{!! Form::open(['route' => 'products.store', 'files'=>true]) !!}
	    			<div class="modal-body">
	    				
	    					<input name="inventory" type="hidden" value="true">
	    					@include('admin.products.fields')
	    				
	    			</div>
	    			<div class="modal-footer">
	                    <!-- Submit Field -->
	                    <div class="form-group col-sm-12">
	                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Uždaryti</button>
	                        {!! Form::submit('Saugoti', ['class' => 'btn btn-primary pull-right']) !!}
	                    </div>    				
	    			</div>
    			{!! Form::close() !!}

    		</div>
    	</div>
    </div>




    <!-- Confirm UZBAIGTI -->
    <div class="modal fade" id="modal-alert-confirm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Prekių Pajamavimas</h4>
                </div>
                <div class="modal-body">
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Atšaukti</button>
                    <button type="button" class="btn btn-primary confirm">Užbaigti</button>
                </div>
            </div>
        </div>
    </div>
    

@endsection