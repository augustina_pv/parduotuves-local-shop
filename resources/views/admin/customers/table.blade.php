<table class="table table-responsive" id="customers-table">
    <thead>
        <tr>
            <th>@lang('base.name')</th>
            <th>@lang('base.lastname')</th>
            <th>@lang('base.company_name')</th>
            <th>@lang('base.address')</th>
            <th>@lang('base.vat')</th>
            <th>@lang('base.image')</th>
            <th colspan="3"></th>
        </tr>
    </thead>
    <tbody>
    @foreach($customers as $customer)
        <tr>
            <td>{!! $customer->name !!}</td>
            <td>{!! $customer->lastname !!}</td>
            <td>{!! $customer->company_name !!}</td>
            <td>{!! $customer->address !!}</td>
            <td>{!! $customer->vat !!}</td>
            <td>{!! $customer->image !!}</td>
            <td>
                {!! Form::open(['route' => ['customers.destroy', $customer->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('customers.show', [$customer->id]) !!}" class='btn btn-default'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('customers.edit', [$customer->id]) !!}" class='btn btn-default'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>