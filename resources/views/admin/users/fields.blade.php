<div class="col-sm-6">
    <!-- Name Field -->
    <div class="form-group ">
        {!! Form::label('name', Lang::get('base.name').':') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Lastname Field -->
    <div class="form-group">
        {!! Form::label('lastname', Lang::get('base.lastname').':') !!}
        {!! Form::text('lastname', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Email Field -->
    <div class="form-group">
        {!! Form::label('email', Lang::get('base.email').':') !!}
        {!! Form::text('email', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Role Field -->
    <div class="form-group">
        {!! Form::label('role_id', Lang::get('base.role').':') !!}
        {{ Form::select('role_id', $roles, null, ['class'=>'form-control']) }}
    </div>

    <!-- Image Field -->
    <div class="form-group">
        {!! Form::label('image', Lang::get('base.image').':') !!}
        @if( !empty($user->image) )
            
            <div class="product-image">
                <img class="small-image" src="@if($user->image) {{ asset('storage') }}/{{ $user->image }} @endif">
            </div>
        @endif
        {!! Form::file('image', null, ['class' => 'form-control']) !!}
    </div>     
</div>

<div class="col-sm-6">

    <!-- Password Field -->
    <div class="form-group">
        {!! Form::label('password', Lang::get('base.password').':') !!}
        {!! Form::password('password', ['class' => 'form-control']) !!}
    </div>

    <!-- Repeat Password Field -->
    <div class="form-group">
        {!! Form::label('password_confirmation', Lang::get('base.password_repeat').':') !!}
        {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
    </div>

    <!-- Permissions Field -->
    <div class="form-group">
        <h3>@lang('base.permissions'):</h3>
        @if($permissions)
            <ul>
                @foreach($permissions as $permission)
                    <li class="category">
                        @php 
                            $checkbox_cat_id = 'permission'.$permission->id
                        @endphp
                        {{ Form::checkbox('permissions[]', $permission->id, null, ['id' => $checkbox_cat_id] ) }}
                        {!! Form::label($checkbox_cat_id, $permission->title) !!}
                    </li>
                @endforeach
            </ul>
        @endif
    </div>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(Lang::get('base.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('users.index') !!}" class="btn btn-default">@lang('base.cancel')</a>
</div>
