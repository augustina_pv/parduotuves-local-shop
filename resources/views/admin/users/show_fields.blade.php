<div class="col-sm-6">

    <!-- Id Field -->
    <div class="form-group">
        {!! Form::label('id', 'Id:') !!}
        <p>{!! $user->id !!}</p>
    </div>

    <!-- Name Field -->
    <div class="form-group">
        {!! Form::label('name', Lang::get('base.name').':') !!}
        <p>{!! $user->name !!}</p>
    </div>

    <!-- Lastname Field -->
    <div class="form-group">
        {!! Form::label('lastname', Lang::get('base.lastname').':') !!}
        <p>{!! $user->lastname !!}</p>
    </div>

    <!-- Email Field -->
    <div class="form-group">
        {!! Form::label('email', Lang::get('base.email').':') !!}
        <p>{!! $user->email !!}</p>
    </div>

    <!-- Role Id Field -->
    <div class="form-group">
        {!! Form::label('role', Lang::get('base.role').':') !!}
        <p>{!! $user->role->name !!}</p>
    </div>

    <!-- Created At Field -->
    <div class="form-group">
        {!! Form::label('created_at', Lang::get('base.created_at').':') !!}
        <p>{!! $user->created_at !!}</p>
    </div>

    <!-- Updated At Field -->
    <div class="form-group">
        {!! Form::label('updated_at', Lang::get('base.updated_at').':') !!}
        <p>{!! $user->updated_at !!}</p>
    </div>
</div>


<div class="col-sm-6">
    <!-- Image Field -->
    <div class="form-group">
        {!! Form::label('image', 'Image:') !!}
        <div class="image">
            <img class="big-image" src="{{ Storage::url($user->image) }}">
        </div>
    </div>

    <!-- Permissions Field -->
    <div class="form-group">
        {!! Form::label('Permissions', 'Permissions:') !!}
        @foreach($user->permissions as $permission)
            <p>{!! $permission->title !!}</p>
        @endforeach  
    </div>

</div>

