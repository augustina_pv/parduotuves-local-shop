<table class="table table-responsive" id="permissions-table">
    <thead>
        <tr>
            <th>@lang('base.title')</th>
            <th>@lang('base.name')</th>
            <th colspan="3"></th>
        </tr>
    </thead>
    <tbody>
    @foreach($permissions as $permissions)
        <tr>
            <td>{!! $permissions->title !!}</td>
            <td>{!! $permissions->name !!}</td>
            <td>
                {!! Form::open(['route' => ['permissions.destroy', $permissions->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('permissions.show', [$permissions->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('permissions.edit', [$permissions->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>