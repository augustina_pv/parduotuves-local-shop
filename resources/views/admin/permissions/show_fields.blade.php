<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $permission->id !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', Lang::get('base.title').':') !!}
    <p>{!! $permission->title !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', Lang::get('base.name').':') !!}
    <p>{!! $permission->name !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', Lang::get('base.crweated_at').':') !!}
    <p>{!! $permission->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', Lang::get('base.updated_at').':') !!}
    <p>{!! $permission->updated_at !!}</p>
</div>

