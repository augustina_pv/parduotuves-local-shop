<!-- Customer Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_id', 'Klientai:') !!}
    {{ Form::select('customer_id', $customers, null, ['class'=>'form-control']) }}
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', 'Price:') !!}
    {!! Form::text('price', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('receipts.index') !!}" class="btn btn-default">Cancel</a>
</div>
