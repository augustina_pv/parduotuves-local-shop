<table class="table table-responsive" id="receipts-table">
    <thead>
        <tr>
            <th>Customer</th>
        <th>Price</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($receipts as $receipt)
        <tr>
            <td>
                {{ $receipt->customer->name }}
            </td>
            <td>{!! $receipt->price !!}</td>
            <td>
                {!! Form::open(['route' => ['receipts.destroy', $receipt->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('receipts.show', [$receipt->id]) !!}" class='btn btn-default'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('receipts.edit', [$receipt->id]) !!}" class='btn btn-default'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>