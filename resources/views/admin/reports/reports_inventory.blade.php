@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">@lang('layouts/menu.report_inventory')</h1>
        <h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('types.create') !!}">Add New</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">

				<table class="table table-responsive" id="types-table">
				    <thead>
				        <tr>
				        	<th>ID</th>
				            <th>@lang('base.title')</th>
				            <th>@lang('base.status')</th>
				            <th>@lang('base.user')</th>
				            <th colspan="3"></th>
				        </tr>
				    </thead>
				    <tbody>


				    @foreach($inventory_activities as $inventory_activity)
				        <tr>
				        	<td>{!! $inventory_activity->id !!}</td>
				            <td>{!! $inventory_activity->title !!}</td>
				            <td>{!! $inventory_activity->status !!}</td>
				            <td>{!! $inventory_activity->user->name!!}</td>
				            <td>
				                <div class='btn-group'>
				                    <a href="{!! route('reports.inventory.show', [$inventory_activity->id]) !!}" class='btn btn-default'><i class="glyphicon glyphicon-eye-open"></i></a>
				                </div>
				            </td>
				        </tr>
				    @endforeach


				    </tbody>
				</table>


            </div>
        </div>
    </div>
@endsection

