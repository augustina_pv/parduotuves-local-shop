@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Produktas
        </h1>
   </section>
   <div class="content">
       @include('flash::message')
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($product, ['route' => ['inventory.update', $product->id], 'method' => 'patch']) !!}

                        <div class="form-group col-sm-12">
                          <div class="form-group">
                              {!! Form::label('count', 'Kiekis:') !!}
                              {!! Form::text('count', null, ['class' => 'form-control']) !!}
                          </div>     
                        </div>

                        <!-- Submit Field -->
                        <div class="form-group col-sm-12">
                            {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
                            <a href="{!! route('inventory.index') !!}" class="btn btn-default">Cancel</a>
                        </div>                        

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection