@extends('layouts.app')


@section('scripts')
    <script>
        $( function() {

            //-------- DATATABLE ------------
            var data_table = $('#data_table').DataTable( {
                "order": [[ 0, "asc" ]], //Prod ID
                "iDisplayLength": 50,
                "language": {
                    "lengthMenu": "Rodyti _MENU_ per puslapį",
                    "search": "Ieškoti prekių:",
                    "info":      "Rodo _START_ iki _END_ iš _TOTAL_ irašai",
                    "infoEmpty": "Rodo 0 iki 0 iš 0 irašai",
                },
                //"columnDefs": [
                //    {
                //        "targets": [ 5 ],
                //        "visible": false,
                //        "searchable": false
                //    }
                //]
            } );
            //------------------------------    


            var _token = $('meta[name="csrf_token"]').attr('content');
            var ajax_url = ajax_param.ajax_url;
            //alert(ajax_url);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': _token
                }
            });


            //------------------------------------------------------------
            $(document).on('click', '#process-inventory-activity', function(){
                start_process_inventory();
            });
            $(document).on('keypress', '#inventory-activity-title', function(e){ 
                if(e.which == 13) {
                    start_process_inventory();
                }
            });            

            $(document).on('click', '#finish-inventory-activity', function(){

                show_alert_confirm("Ar tikrai norite užbaigti šio dokumento operaciją?");  
            });

            function show_alert_confirm(text){
                $('#modal-alert-confirm .modal-body').html(text);
                $('#modal-alert-confirm').modal('toggle');      
            }               

            $(document).on('click', '#modal-alert-confirm .confirm', function(){

                $('#modal-alert-confirm').modal('toggle'); //close alert confirm
                finish_process_inventory();  
            }); 
            //-------------------------------------------------------------

            
            //$(document).on('change paste keyup', '.real-count', function(){ });
     
            

  
            function start_process_inventory(){
                var process_btn = $('#process-inventory-activity');
                var finish_btn = $('#finish-inventory-activity');
                var title_obj = $('#inventory-activity-title');
                var inventory_datatable = $('.inventory-datatable');
                var message_obj = $('#inventory_message');
                message_obj.html('');

                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: ajax_url,
                    data: {
                        action: 'start_process_inventory_activity',
                        title: title_obj.val()
                    },
                    success: function(data){
                        console.log(data);
                        if(data.saved){
                            process_btn.attr('disabled', true); //off
                            title_obj.attr('disabled', true);   //off
                            //title_obj.val('');
                            finish_btn.attr('disabled', false); //on

                            inventory_datatable.removeClass('hidden');
                            inventory_datatable.attr('activity-id', data.activity_id);
                        }

                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) { 
                        console.log(XMLHttpRequest); 
                        console.log("Status: " + textStatus); 
                        console.log("Error: " + errorThrown);
                        /*
                        message_obj.html(
                            '<div class="alert alert-danger">'+textStatus + ' ' + errorThrown+
                            ' try to <a href="pajamavimas">refresh</a></div>'
                        );
                        */ 
                    }               
                });                
            }

            function finish_process_inventory(){

                console.log('finish inventory');

                var process_btn = $('#process-inventory-activity');
                var finish_btn = $('#finish-inventory-activity');
                var title_obj = $('#inventory-activity-title');
                var title = title_obj.val();
                var inventory_datatable = $('.inventory-datatable'); 
                var message_obj = $('#inventory_message');
                var products = [];


                //--- collect products (real count)
                data_table.rows('.product-row').nodes().to$().each(function(){
                    var row = $(this);
                    var prod_id = row.attr('prod');
                    var price = row.find('.price').html();
                    var count = row.find('.count').html();
                    var real_count = row.find('.real-count').val();
 
                    var product = {
                        prod_id: prod_id,
                        price: price,
                        count: count,
                        real_count: real_count
                    };
                    products.push(product);  
                });
                
                //---------------------------------
                
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: ajax_url,
                    data: {
                        action: 'finish_process_inventory_activity',
                        activity_id: inventory_datatable.attr('activity-id'),
                        products: products
                    },
                    success: function(data){
                        console.log(data);
                        if(data.saved){
                            
                            title_obj.val('');
                            title_obj.attr('disabled', false);  //on
                            process_btn.attr('disabled', false); //on
                            finish_btn.attr('disabled', true);  //off


                            inventory_datatable.addClass('hidden');

                            message_obj.html(
                                '<div class="alert alert-success">Inventorizacija <b>'+title+'</b> sėkmingai įrašyta</div>'
                            );                             
                        }
                        else{
                            message_obj.html(
                                '<div class="alert alert-danger">Error ' +
                                ' try to <a href="inventory">refresh</a></div>'
                            );                             
                        }

                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) { 
                        console.log(XMLHttpRequest); 
                        console.log("Status: " + textStatus); 
                        console.log("Error: " + errorThrown);

                        message_obj.html(
                            '<div class="alert alert-danger">'+textStatus + ' ' + errorThrown+
                            ' try to <a href="inventory">refresh</a></div>'
                        );                         
                    }               
                });
                
            }

        });  



    </script>
@endsection


@section('content')
    <section class="content-header">
        <h1 class="pull-left">@lang('layouts/menu.inventory')</h1>

    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="row">
            <div class="col-sm-12">
                <div id="inventory_message"></div>
            </div>
        </div>

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">

                <div class="inventory-activity-inputs">
                    <div class="col-sm-9">
                        <div class="form-group">
                            <label for="inventory-activity-title">@lang('base.title'):</label>
                            <input id="inventory-activity-title" type="text" name="title" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="buttons-group pull-left">
                            <button id="process-inventory-activity" class="btn btn-success">Vykdyti</button>
                        </div>
                        <div class="buttons-group pull-right">
                            <button id="finish-inventory-activity" class="btn btn-danger" disabled="true">Irašyti</button>
                        </div>                        
                    </div>
                </div>

                <div class="inventory-datatable hidden">
                    <table id="data_table" class="table table-hover" style="width: 100%!important">
                        <thead>
                            <tr>
                                <th>@lang('base.product_id')</th>
                                <th>@lang('base.product')</th>
                                <th>@lang('base.product_code')</th>
                                <th>@lang('base.count')</th>
                                <th>@lang('base.count_real')</th>
                                <th>@lang('base.price')</th>
                                <th>@lang('base.total')</th>
                                <th style="display: none"></th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($products as $product)
                                <tr prod="{{ $product->id }}" class="product-row">
                                    <td>{{ $product->id }}</td>
                                    <td>{{ $product->title }}</td>
                                    <td>{{ $product->sku }}</td>
                                    <td class="count">{{ $product->count }}</td>
                                    <td>
                                        <input class="real-count" type="text" name="real-count" value="{{ $product->count }}">
                                    </td>
                                    <td class="price">{{ $product->price }}</td>
                                    <td>{{ $product->price_total }}</td>
                                    <td style="display: none">
                                        <div class='btn-group'>
                                            <a href="{!! route('inventory.edit', [$product->id]) !!}" class='btn btn-default'><i class="glyphicon glyphicon-edit"></i></a>
                                        </div>
                                    </td>                                
                                </tr>
                            @endforeach
                        </tbody>

                        <tfoot>
                            <tr>
                                <td colspan="3"><b>@lang('base.totals')</b></td>
                                <td colspan="2"><b>{{ $total_count }}</b></td>
                                <td colspan="2"><b>{{ $total_price }}</b></td>
                            </tr>
                        </tfoot> 
                       
                    </table>
                </div>

            </div>
        </div>
    </div>


    <!-- Confirm UZBAIGTI -->
    <div class="modal fade" id="modal-alert-confirm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Inventorizacija</h4>
                </div>
                <div class="modal-body">
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Atšaukti</button>
                    <button type="button" class="btn btn-primary confirm">Užbaigti</button>
                </div>
            </div>
        </div>
    </div>    
@endsection

