@extends('layouts.app')

@section('scripts')
    <script>
        $( function() {

            //-------- DATATABLE ------------
            $('#data_table').DataTable( {
                "order": [[ 5, "desc" ]], //Prod ID
                "iDisplayLength": 50,
                "language": {
                    "lengthMenu": "Rodyti _MENU_ per puslapį",
                    "search": "Ieškoti prekių:",
                }
            } );
            //------------------------------           
        });         
    </script>
@endsection


@section('content')
    <section class="content-header">
        <h1>
            Supplier Document
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px; padding-bottom: 50px">
                    @include('admin.supplier_documents.show_fields')
                    <a href="{!! route('supplierDocuments.index') !!}" class="btn btn-default">Back</a>
                </div>



             
                    <table id="data_table" class="table table-hover">
                        <thead>
                            <tr>
                                <th>Produktas</th>
                                <th>Tipas</th>
                                <th>Vartotojas</th>
                                <th>Kiekis</th>
                                <th>Kaina</th>
                                <th>Data</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($supplierDocument->transactions as $transaction)
                                <tr>
                                    <td>{{ $transaction->product->title }}</td>
                                    <td>{{ $transaction->type->name }}</td>
                                    <td>{{ $transaction->user->name }}</td>
                                    <td>{{ $transaction->count }}</td>
                                    <td>{{ $transaction->price }}</td>
                                    <td>{{ $transaction->created_at }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
            


            </div>
        </div>
    </div>
@endsection
