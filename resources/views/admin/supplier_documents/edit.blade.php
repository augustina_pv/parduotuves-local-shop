@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            @lang('layouts/menu.supplier_document')
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($supplierDocument, ['route' => ['supplierDocuments.update', $supplierDocument->id], 'method' => 'patch']) !!}

                        @include('admin.supplier_documents.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection