<!-- Nr Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nr', Lang::get('base.nr').':') !!}
    {!! Form::text('nr', null, ['class' => 'form-control']) !!}
</div>

<!-- Supplier Field -->
<div class="form-group col-sm-6">
    {!! Form::label('supplier_id', Lang::get('base.supplier').':') !!}
    {{ Form::select('supplier_id', $suppliers, null, ['class'=>'form-control', 'placeholder' => '']) }}
</div>


<!-- Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date', Lang::get('base.date').':') !!}
    {!! Form::date('date', \Carbon\Carbon::now(), ['class' => 'form-control']  )!!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(Lang::get('base.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('supplierDocuments.index') !!}" class="btn btn-default">@lang('base.cancel')</a>
</div>
