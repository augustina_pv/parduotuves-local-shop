<table class="table table-responsive" id="supplierDocuments-table">
    <thead>
        <tr>
            <th>ID</th>
            <th>@lang('base.nr')</th>
            <th>@lang('base.supplier')</th>
            <th>@lang('base.products_count')</th>
            <th>@lang('base.date')</th>
            <th colspan="3"></th>
        </tr>
    </thead>
    <tbody>
    @foreach($supplierDocuments as $supplierDocument)
        <tr>
            <td>{!! $supplierDocument->id !!}</td>
            <td>{!! $supplierDocument->nr !!}</td>
            <td>
                @if(!empty($supplierDocument->supplier))
                    {{ $supplierDocument->supplier->title }}
                @endif
            </td>
            <td>{!! $supplierDocument->transactions->count() !!}</td>
            <td> {!! date('Y-m-d', strtotime($supplierDocument->date)) !!} </td>
            <td>
                {!! Form::open(['route' => ['supplierDocuments.destroy', $supplierDocument->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('supplierDocuments.show', [$supplierDocument->id]) !!}" class='btn btn-default'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('supplierDocuments.edit', [$supplierDocument->id]) !!}" class='btn btn-default'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>