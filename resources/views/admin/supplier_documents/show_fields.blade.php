<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $supplierDocument->id !!}</p>
</div>

<!-- Nr Field -->
<div class="form-group">
    {!! Form::label('nr', 'Nr:') !!}
    <p>{!! $supplierDocument->nr !!}</p>
</div>

<!-- Supplier Id Field -->
<div class="form-group">
    {!! Form::label('supplier_id', 'Supplier Id:') !!}
    <p>{!! $supplierDocument->supplier_id !!}</p>
</div>

<!-- Supplier Title Field -->
<div class="form-group">
    {!! Form::label('supplier_title', 'Supplier:') !!}
    <p>{!! $supplierDocument->supplier->title !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $supplierDocument->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $supplierDocument->updated_at !!}</p>
</div>

