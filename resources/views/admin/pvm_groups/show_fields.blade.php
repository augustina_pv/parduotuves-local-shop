<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $pvmGroup->id !!}</p>
</div>

<!-- Pos Option Field -->
<div class="form-group">
    {!! Form::label('pos_option', Lang::get('base.pos_option').':') !!}
    <p>{!! $pvmGroup->pos_option !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', Lang::get('base.title').':') !!}
    <p>{!! $pvmGroup->title !!}</p>
</div>

<!-- Value Field -->
<div class="form-group">
    {!! Form::label('title', Lang::get('base.value').':') !!}
    <p>{!! $pvmGroup->value !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', Lang::get('base.created_at').':') !!}
    <p>{!! $pvmGroup->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', Lang::get('base.updated_at').':') !!}
    <p>{!! $pvmGroup->updated_at !!}</p>
</div>

