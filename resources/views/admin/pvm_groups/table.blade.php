<table class="table table-responsive" id="pvmGroups-table">
    <thead>
        <tr>
            <th>ID</th>
            <th>@lang('base.pos_option')</th>
            <th>@lang('base.title')</th>
            <th>@lang('base.value')</th>
            <th colspan="3"></th>
        </tr>
    </thead>
    <tbody>
    @foreach($pvmGroups as $pvmGroup)
        <tr>
            <td>{!! $pvmGroup->id !!}</td>
            <td>{!! $pvmGroup->pos_option !!}</td>
            <td>{!! $pvmGroup->title !!}</td>
            <td>{!! number_format($pvmGroup->value, 2) !!}</td>
            <td>
                {!! Form::open(['route' => ['pvmGroups.destroy', $pvmGroup->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('pvmGroups.show', [$pvmGroup->id]) !!}" class='btn btn-default'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('pvmGroups.edit', [$pvmGroup->id]) !!}" class='btn btn-default'><i class="glyphicon glyphicon-edit"></i></a>

                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger hidden', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>