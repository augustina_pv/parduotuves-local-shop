@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            @lang('layouts/menu.pvm_group')
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'pvmGroups.store']) !!}

                        @include('admin.pvm_groups.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
