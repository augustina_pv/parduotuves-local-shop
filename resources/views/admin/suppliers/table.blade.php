<table class="table table-responsive" id="suppliers-table">
    <thead>
        <tr>
            <th>@lang('base.title')</th>
            <th>@lang('base.address')</th>
            <th>@lang('base.name')</th>
            <th>@lang('base.lastname')</th>
            <th>@lang('base.phone')</th>
            <th>@lang('base.code')</th>
            <th>@lang('base.pvm_code')</th>
            <th>@lang('base.documents')</th>

            <th colspan="3"></th>
        </tr>
    </thead>
    <tbody>
    @foreach($suppliers as $supplier)
        <tr>
            <td>{!! $supplier->title !!}</td>
            <td>{!! $supplier->address !!}</td>
            <td>{!! $supplier->name !!}</td>
            <td>{!! $supplier->lastname !!}</td>
            <td>{!! $supplier->phone !!}</td>
            <td>{!! $supplier->code !!}</td>
            <td>{!! $supplier->pvm_code !!}</td>

            <td>
                
                @foreach($supplier->supplier_documents as $document)
                    <a href="{{route('supplierDocuments.index')}}/{{$document->id}}"><div class="categories-inline">{{ $document->nr }}</div></a>
                @endforeach
            </td>

            <td>
                {!! Form::open(['route' => ['suppliers.destroy', $supplier->id], 'method' => 'delete']) !!}
                <div class='btn-group'>

                    <a href="{!! route('suppliers.show', [$supplier->id]) !!}" class='btn btn-default'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('suppliers.edit', [$supplier->id]) !!}" class='btn btn-default'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>