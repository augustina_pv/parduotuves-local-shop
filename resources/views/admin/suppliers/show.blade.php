@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            @lang('layouts/menu.supplier')
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">

            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('admin.suppliers.show_fields')
                    
                </div>
            </div>

        </div>
    </div>

    <div id="transactions">
        <section class="content-header">
            <h1>
                @lang('base.documents') (@lang('layouts/menu.transactions'))
            </h1>
        </section>
        <div  class="content">
            <div class="box box-primary">
          
                <div class="box-body">
                    <div class="row" style="padding-left: 20px">
                        @include('admin.supplier_documents.table')
                        <a href="{!! route('suppliers.index') !!}" class="btn btn-default">@lang('base.back')</a>
                    </div>
                </div>
           
            </div>
        </div>
    </div>

    <!--
    <div id="products-as-transactions">
        <section class="content-header">
            <h1>
                Transactions as products
            </h1>
        </section>
        <div  class="content">
            <div class="box box-primary">
          
                <div class="box-body">
                    <div class="row" style="padding-left: 20px">
                        @include('admin.suppliers.transactions.table')
                        <a href="{!! route('suppliers.index') !!}" class="btn btn-default">Back</a>
                    </div>
                </div>
           
            </div>
        </div>
    </div>  
    -->     
  
@endsection
