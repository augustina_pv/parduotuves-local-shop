<table class="table table-responsive" id="products-table">
    <thead>
        <tr>
            <th>Pavadinimas</th>
            <th>Doc ID</th>
            <th>Kategorijos</th>
            <th>Sku</th>
            <th>Barkodas</th>
            <th>Pirkimo Kaina</th>
            <th>Kaina</th>
            <th>Kaina Pvm</th>
            <th>Pvm Grupė</th>
            <th>Kiekis</th>
            <th>Matas</th>
            <th>Paveikslėlis</th>
            <th colspan="3"></th>
        </tr>
    </thead>
    <tbody>
    @foreach($transactions as $transaction)
        <tr>
            <td>{!! $transaction->product->title !!}</td>
            <td>{!! $transaction->document_id !!}</td>
            <td>
                @foreach($transaction->product->categories as $category)
                    <a href="{{route('categories.index')}}/{{$category->id}}">
                        <div class="categories-inline">{{ $category->title }}</div>
                    </a>
                @endforeach
            </td>
            <td>{!! $transaction->product->sku !!}</td>
            <td>{!! $transaction->product->barcode !!}</td>
            <td>{{ $transaction->product->price_purchase }}</td>
            <td>{!! number_format($transaction->price, 2) !!}</td>
            <td>{{ get_price_pvm($transaction->price) }}</td>
            <td>{{ $transaction->product->pvm_group['title'] }}</td>
            <td>{!! $transaction->count !!}</td>
            <td>{{ $transaction->product->measure['title'] }}</td>
            <td>
                @if( !empty($transaction->product->image) )
                    <img class="small-image" src="{{ Storage::url($transaction->product->image) }}">
                @endif
            </td>
            <td>
                {!! Form::open(['route' => ['transactions.destroy', $transaction->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('transactions.show', [$transaction->id]) !!}" class='btn btn-default'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('transactions.edit', [$transaction->id]) !!}" class='btn btn-default'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>