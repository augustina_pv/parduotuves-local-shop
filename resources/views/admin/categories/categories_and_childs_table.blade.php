@if(count($categories))

	@php 
		$prefix .= '<span class="prefix"></span>'
	@endphp 

	@foreach($categories as $category)

		@php $first_class = '' @endphp			

		@if($category->parent_id === 0)
			@php $prefix = '' @endphp 
			@php $first_class = 'first'	@endphp
		@endif

	    <tr class="category-row {{ $first_class  }}">
	        <td>{!! $category->id !!}</td>
	        <td class="name">
	        	{!! $prefix !!} {!! $category->title !!}
	        </td>
	        <td>{!! $category->parent_id !!}</td>
	        <td>
	            @if( !empty($category->image) )
	                <img class="small-image" src="{{ asset('storage') }}/{{ $category->image }}">
	            @endif
	        </td>
	        <td>{!! $category->ordering !!}</td>
	        <td>
	            {!! Form::open(['route' => ['categories.destroy', $category->id], 'method' => 'delete']) !!}
	            <div class='btn-group'>
	                <a href="{!! route('categories.show', [$category->id]) !!}" class='btn btn-default'><i class="glyphicon glyphicon-eye-open"></i></a>
	                <a href="{!! route('categories.edit', [$category->id]) !!}" class='btn btn-default'><i class="glyphicon glyphicon-edit"></i></a>
	                {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
	            </div>
	            {!! Form::close() !!}
	        </td>
	    </tr>

	    @include('admin.categories.categories_and_childs_table', ['categories' => $category->childs, 'prefix' => $prefix])

    @endforeach

@endif