<table class="table table-responsive" id="categories-table">
    <thead>
        <tr>
            <th>ID</th>
            <th>@lang('base.title')</th>
            <th>@lang('base.parent_id')</th>
            <th>@lang('base.image')</th>
            <th>@lang('base.ordering')</th>
            <th colspan="3"></th>
        </tr>
    </thead>
    <tbody>

        @include('admin.categories.categories_and_childs_table', ['categories' => $categories, 'prefix' => ''])

    </tbody>
</table>
