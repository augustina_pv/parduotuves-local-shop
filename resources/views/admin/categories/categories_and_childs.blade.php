@if(count($categories))
	<ul>
	    @foreach($categories as $category)
	        <li>{{ $category->id }} {{ $category->title }}</li>
	        @include('categories.categories_and_childs',['categories' => $category->childs])
	    @endforeach
	</ul>
@endif