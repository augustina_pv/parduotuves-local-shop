
<div class="col-sm-4">
    <!-- Id Field -->
    <div class="form-group">
        {!! Form::label('id', 'Id:') !!}
        <p>{!! $category->id !!}</p>
    </div>

    <!-- Title Field -->
    <div class="form-group">
        {!! Form::label('title', Lang::get('base.title').':') !!}
        <p>{!! $category->title !!}</p>
    </div>

    <!-- Parent Id Field -->
    <div class="form-group">
        {!! Form::label('parent_id', Lang::get('base.parent_id').':') !!}
        <p>{!! $category->parent_id !!}</p>
    </div>

    <!-- Ordering Field -->
    <div class="form-group">
        {!! Form::label('ordering', Lang::get('base.ordering').':') !!}
        <p>{!! $category->ordering !!}</p>
    </div>

    <!-- Created At Field -->
    <div class="form-group">
        {!! Form::label('created_at', Lang::get('base.created_at').':') !!}
        <p>{!! $category->created_at !!}</p>
    </div>

    <!-- Updated At Field -->
    <div class="form-group">
        {!! Form::label('updated_at', Lang::get('base.updated_at').':') !!}
        <p>{!! $category->updated_at !!}</p>
    </div>
</div>

<div class="col-sm-8">
    <!-- Image Field -->
    <div class="form-group">
        {!! Form::label('image', Lang::get('base.image').':') !!}
        <div class="image">
            <img class="big-image" src="{{ asset('storage') }}/{{ $category->image }}">
        </div> 
    </div>
</div>
