<div class="modal fade" id="payment-modal-id">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<div class="message" v-html="message"></div>
			</div>
			<div class="modal-body">


				<div v-if="products_list.length == 0">
					<div class="price-change"><b>Graža:</b> <% money_change2 %></div>
				</div>

				<div class="row" v-else>

					<div class="col-sm-6">
						<div class="price-total"><b>Mokėti:</b> <% price_total %></div>
					</div>

					<div class="col-sm-6">
						<div class="price-change"><b>Graža:</b> <% money_change %></div>
					</div>


					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
					    <div class="form-group">
						    <label for="usr">Gryni:</label>
						    <input disabled type="text" class="form-control" id="payment-cash" v-model="money_cash">
					    </div>						
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
					    <div class="form-group">
						    <label for="usr">Kortelė:</label>
						    <input type="checkbox" name="card-enable" value="1" v-on:click="enable_card">Enable Card<br>
						    <input disabled type="text" class="form-control" id="payment-card" v-model="money_card">
					    </div>							
					</div>

					<div class="col-xs-12">
						<div class="calculator-modal">

								<div class="buttons">
									<div class="button btn btn-default" v-on:click="enter_money(1)">1</div>
									<div class="button btn btn-default" v-on:click="enter_money(2)">2</div>
									<div class="button btn btn-default" v-on:click="enter_money(3)">3</div>
									<div class="button btn btn-default" v-on:click="enter_money(4)">4</div>
									<div class="button btn btn-default" v-on:click="enter_money(5)">5</div>
									<div class="button btn btn-default" v-on:click="enter_money(6)">6</div>
									<div class="button btn btn-default" v-on:click="enter_money(7)">7</div>
									<div class="button btn btn-default" v-on:click="enter_money(8)">8</div>
									<div class="button btn btn-default" v-on:click="enter_money(9)">9</div>
									<div class="button btn btn-default" v-on:click="enter_money(0)">0</div>
									<div class="button btn btn-default" v-on:click="delete_money()">Del</div>
									<div class="button btn btn-default" v-on:click="enter_money(100)">00</div>
								</div>
							</div>

					</div>

					<div class="col-xs-12">
						<div v-if="show_pay_button">
							<button class="btn btn-success btn-pay" v-on:click="save_order()" >PAY</button>
						</div>
					</div>
				</div>



				
			</div>
			<div class="modal-footer">

			</div>
		</div>
	</div>
</div>