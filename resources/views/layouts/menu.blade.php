

    <li class="">
        <a target="blank" href="{!! route('shop-app-js') !!}">
            <i class="fa fa-hand-pointer-o"></i>
            <span>@lang('layouts/menu.point_of_sale')</span>
        </a>
    </li>



@can('view-goods-receiving')
    <li class="{{ Request::is('pajamavimas*') ? 'active' : '' }}">
        <a href="{!! route('pajamavimas.index') !!}"><i class="fa fa-hand-lizard-o"></i>
            <span>@lang('layouts/menu.goods_receiving')</span>
        </a>
    </li>
@endcan

@can('view-inventory')
    <li class="{{ Request::is('inventory*') ? 'active' : '' }}">
        <a href="{!! route('inventory.index') !!}"><i class="fa fa-hand-lizard-o"></i>
            <span>@lang('layouts/menu.inventory')</span>
        </a>
    </li>
@endcan


@can('view-reports')
    <li class="{{ Request::is('reports*') ? 'active' : '' }}">
        <a href="{!! route('reports.index') !!}"><i class="fa fa-arrows-h"></i>
            <span>@lang('layouts/menu.reports')</span>
        </a>
    </li>

    <li class="{{ Request::is('reports*') ? 'active' : '' }}">
        <a href="{!! route('reports.purchase') !!}"><i class="fa fa-arrow-left"></i>
            <span>@lang('layouts/menu.report_purchase')</span>
        </a>
    </li>    

    <li class="{{ Request::is('reports*') ? 'active' : '' }}">
        <a href="{!! route('reports.sale') !!}"><i class="fa fa-arrow-right"></i>
            <span>@lang('layouts/menu.report_sale')</span>
        </a>
    </li>

    <li class="{{ Request::is('reports*') ? 'active' : '' }}">
        <a href="{!! route('reports.inventory') !!}"><i class="fa fa-arrow-right"></i>
            <span>@lang('layouts/menu.report_inventory')</span>
        </a>
    </li>      
@endcan


<li class="menu-block"></li>

@can('view-transactions')
    <li class="{{ Request::is('transactions*') ? 'active' : '' }}">
        <a href="{!! route('transactions.index') !!}"><i class="fa fa-check"></i>
            <span>@lang('layouts/menu.transactions')</span>
        </a>
    </li>
@endcan

@can('view-types')
    <li class="{{ Request::is('types*') ? 'active' : '' }}">
        <a href="{!! route('types.index') !!}"><i class="fa fa-file-o"></i>
            <span>@lang('layouts/menu.types')</span>
        </a>
    </li>
@endcan



<li class="menu-block"></li>

@can('view-products')
    <li class="{{ Request::is('products*') ? 'active' : '' }}">
        <a href="{!! route('products.index') !!}"><i class="fa fa-circle-o"></i>
            <span>@lang('layouts/menu.products')</span>
        </a>
    </li>
@endcan


@can('view-categories')
    <li class="{{ Request::is('categories*') ? 'active' : '' }}">
        <a href="{!! route('categories.index') !!}"><i class="fa fa-certificate"></i>
            <span>@lang('layouts/menu.categories')</span>
        </a>
    </li>
@endcan

@can('view-pvm-groups')
    <li class="{{ Request::is('pvmGroups*') ? 'active' : '' }}">
        <a href="{!! route('pvmGroups.index') !!}"><i class="fa fa-object-ungroup"></i>
            <span>@lang('layouts/menu.pvm_groups')</span>
        </a>
    </li>
@endcan

@can('view-measures')
    <li class="{{ Request::is('measures*') ? 'active' : '' }}">
        <a href="{!! route('measures.index') !!}"><i class="fa fa-renren"></i>
            <span>@lang('layouts/menu.measures')</span>
        </a>
    </li>
@endcan


<li class="menu-block"></li>

@can('view-suppliers')
    <li class="{{ Request::is('suppliers*') ? 'active' : '' }}">
        <a href="{!! route('suppliers.index') !!}"><i class="fa fa-truck"></i>
            <span>@lang('layouts/menu.suppliers')</span>
        </a>
    </li>
@endcan

@can('view-supplier-documents')
    <li class="{{ Request::is('supplierDocuments*') ? 'active' : '' }}">
        <a href="{!! route('supplierDocuments.index') !!}"><i class="fa fa-columns"></i>
            <span>@lang('layouts/menu.supplier_documents')</span>
        </a>
    </li>
@endcan


<li class="menu-block"></li>

@can('view-customers')
    <li class="{{ Request::is('customers*') ? 'active' : '' }}">
        <a href="{!! route('customers.index') !!}"><i class="fa fa-user"></i>
            <span>@lang('layouts/menu.customers')</span>
        </a>
    </li>
@endcan

@can('view-receipts-nerodom')
    <li class="{{ Request::is('receipts*') ? 'active' : '' }}">
        <a href="{!! route('receipts.index') !!}"><i class="fa fa-file-text-o"></i>
            <span>@lang('layouts/menu.receipts')</span>
        </a>
    </li>
@endcan


@can('view-orders')
    <li class="{{ Request::is('orders*') ? 'active' : '' }}">
        <a href="{!! route('orders.index') !!}"><i class="fa fa-file-text-o"></i>
            <span>@lang('layouts/menu.orders')</span>
        </a>
    </li>
@endcan


<li class="menu-block"></li>

@can('view-inner-documents')
    <li class="{{ Request::is('innerDocuments*') ? 'active' : '' }}">
        <a href="{!! route('innerDocuments.index') !!}"><i class="fa fa-edit"></i>
            <span>@lang('layouts/menu.inner_documents')</span>
        </a>
    </li>
@endcan



<li class="menu-block"></li>

@can('view-users')
    <li class="{{ Request::is('users*') ? 'active' : '' }}">
        <a href="{!! route('users.index') !!}"><i class="fa fa-users"></i>
            <span>@lang('layouts/menu.users')</span>
        </a>
    </li>
@endcan

@can('view-roles')
    <li class="{{ Request::is('roles*') ? 'active' : '' }}">
        <a href="{!! route('roles.index') !!}"><i class="fa fa-wrench"></i>
            <span>@lang('layouts/menu.roles')</span>
        </a>
    </li>
@endcan

@can('view-permissions')
    <li class="{{ Request::is('permissions*') ? 'active' : '' }}">
        <a href="{!! route('permissions.index') !!}"><i class="fa fa-edit"></i>
            <span>@lang('layouts/menu.permissions')</span>
        </a>
    </li>
@endcan


<li class="menu-block"></li>

<li class="{{ Request::is('trash*') ? 'active' : '' }}">
    <a href="{!! route('trash.index') !!}"><i class="fa fa-trash"></i>
        <span>@lang('layouts/menu.trash')</span>
    </a>
</li>

