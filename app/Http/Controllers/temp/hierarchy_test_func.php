    private function generateList($data, $id, &$list) {
            foreach($data as $d) {
                if($d[2] == $id) {          
                    $list[] = $d; // Child found, add it to list            
                    $this->generateList($data, $d[0], $list); // Now search for childs of this child
                }
            }
    }

    public function index2(){

        $data = [
            [1,'Bob',''], 
            [2,'Sam',''],
            [3,'Mic',''],
            [4,'Simon',1], 
            [5,'John',''],
            [6, 'Tom',1], 
            [7,'Phillip',3],
            [8,'James',7]
        ];

        $list = [];
        $this->generateList($data, '', $list);

        var_dump($list);


    }