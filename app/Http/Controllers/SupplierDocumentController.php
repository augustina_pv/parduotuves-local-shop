<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSupplierDocumentRequest;
use App\Http\Requests\UpdateSupplierDocumentRequest;
use App\Repositories\SupplierDocumentRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

use App\Models\Supplier;
use App\Models\SupplierDocument;
//use App\Models\Transactions;

class SupplierDocumentController extends AppBaseController
{
    /** @var  SupplierDocumentRepository */
    private $supplierDocumentRepository;

    public function __construct(SupplierDocumentRepository $supplierDocumentRepo)
    {
        $this->middleware('auth');

        $this->middleware('can:view-supplier-documents');

        $this->supplierDocumentRepository = $supplierDocumentRepo;
    }

    /**
     * Display a listing of the SupplierDocument.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->supplierDocumentRepository->pushCriteria(new RequestCriteria($request));
        $supplierDocuments = $this->supplierDocumentRepository->all();

        return view('admin.supplier_documents.index')
            ->with('supplierDocuments', $supplierDocuments);
    }

    /**
     * Show the form for creating a new SupplierDocument.
     *
     * @return Response
     */
    public function create()
    {
        $suppliers = Supplier::pluck('title', 'id');

        $data = [
            'suppliers' => $suppliers
        ];

        return view('admin.supplier_documents.create', $data);
    }

    /**
     * Store a newly created SupplierDocument in storage.
     *
     * @param CreateSupplierDocumentRequest $request
     *
     * @return Response
     */
    public function store(CreateSupplierDocumentRequest $request)
    {
        $input = $request->all();

        $supplierDocument = $this->supplierDocumentRepository->create($input);

        Flash::success('Supplier Document saved successfully.');

        return redirect(route('supplierDocuments.index'));
    }

    /**
     * Display the specified SupplierDocument.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $supplierDocument = $this->supplierDocumentRepository->findWithoutFail($id);

        if (empty($supplierDocument)) {
            Flash::error('Supplier Document not found');

            return redirect(route('supplierDocuments.index'));
        }



        //$transactions = Transaction::where();

        return view('admin.supplier_documents.show')->with('supplierDocument', $supplierDocument);
    }

    /**
     * Show the form for editing the specified SupplierDocument.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $supplierDocument = $this->supplierDocumentRepository->findWithoutFail($id);

        if (empty($supplierDocument)) {
            Flash::error('Supplier Document not found');

            return redirect(route('supplierDocuments.index'));
        }

        $suppliers = Supplier::pluck('title', 'id');

        $data = [
            'supplierDocument' => $supplierDocument,
            'suppliers' => $suppliers
        ];

        return view('admin.supplier_documents.edit', $data);
    }

    /**
     * Update the specified SupplierDocument in storage.
     *
     * @param  int              $id
     * @param UpdateSupplierDocumentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSupplierDocumentRequest $request)
    {
        //$supplierDocument = $this->supplierDocumentRepository->findWithoutFail($id);
        $supplierDocument = SupplierDocument::find($id);

        if (empty($supplierDocument)) {
            Flash::error('Supplier Document not found');

            return redirect(route('supplierDocuments.index'));
        }

        //$supplierDocument = $this->supplierDocumentRepository->update($request->all(), $id);

        if($request->nr)
            $supplierDocument->nr = $request->nr;

        if($request->date)
            $supplierDocument->date = $request->date;        

        $supplierDocument->supplier_id = $request->supplier_id or null;

            
        $supplierDocument->update();       


        Flash::success('Supplier Document updated successfully.');

        return redirect(route('supplierDocuments.index'));
    }

    /**
     * Remove the specified SupplierDocument from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $supplierDocument = $this->supplierDocumentRepository->findWithoutFail($id);

        if (empty($supplierDocument)) {
            Flash::error('Supplier Document not found');

            return redirect(route('supplierDocuments.index'));
        }

        $this->supplierDocumentRepository->delete($id);

        Flash::success('Supplier Document deleted successfully.');

        return redirect(route('supplierDocuments.index'));
    }
}
