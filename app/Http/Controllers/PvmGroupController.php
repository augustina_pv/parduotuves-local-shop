<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePvmGroupRequest;
use App\Http\Requests\UpdatePvmGroupRequest;
use App\Repositories\PvmGroupRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class PvmGroupController extends AppBaseController
{
    /** @var  PvmGroupRepository */
    private $pvmGroupRepository;

    public function __construct(PvmGroupRepository $pvmGroupRepo)
    {
        $this->middleware('auth');

        $this->middleware('can:view-pvm-groups');

        $this->pvmGroupRepository = $pvmGroupRepo;
    }

    /**
     * Display a listing of the PvmGroup.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->pvmGroupRepository->pushCriteria(new RequestCriteria($request));
        $pvmGroups = $this->pvmGroupRepository->all();

        return view('admin.pvm_groups.index')
            ->with('pvmGroups', $pvmGroups);
    }

    /**
     * Show the form for creating a new PvmGroup.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.pvm_groups.create');
    }

    /**
     * Store a newly created PvmGroup in storage.
     *
     * @param CreatePvmGroupRequest $request
     *
     * @return Response
     */
    public function store(CreatePvmGroupRequest $request)
    {
        $input = $request->all();

        $pvmGroup = $this->pvmGroupRepository->create($input);

        Flash::success('Pvm Group saved successfully.');

        return redirect(route('pvmGroups.index'));
    }

    /**
     * Display the specified PvmGroup.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $pvmGroup = $this->pvmGroupRepository->findWithoutFail($id);

        if (empty($pvmGroup)) {
            Flash::error('Pvm Group not found');

            return redirect(route('pvmGroups.index'));
        }

        return view('admin.pvm_groups.show')->with('pvmGroup', $pvmGroup);
    }

    /**
     * Show the form for editing the specified PvmGroup.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $pvmGroup = $this->pvmGroupRepository->findWithoutFail($id);

        if (empty($pvmGroup)) {
            Flash::error('Pvm Group not found');

            return redirect(route('pvmGroups.index'));
        }

        return view('admin.pvm_groups.edit')->with('pvmGroup', $pvmGroup);
    }

    /**
     * Update the specified PvmGroup in storage.
     *
     * @param  int              $id
     * @param UpdatePvmGroupRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePvmGroupRequest $request)
    {
        $pvmGroup = $this->pvmGroupRepository->findWithoutFail($id);

        if (empty($pvmGroup)) {
            Flash::error('Pvm Group not found');

            return redirect(route('pvmGroups.index'));
        }

        $pvmGroup = $this->pvmGroupRepository->update($request->all(), $id);

        Flash::success('Pvm Group updated successfully.');

        return redirect(route('pvmGroups.index'));
    }

    /**
     * Remove the specified PvmGroup from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $pvmGroup = $this->pvmGroupRepository->findWithoutFail($id);

        if (empty($pvmGroup)) {
            Flash::error('Pvm Group not found');

            return redirect(route('pvmGroups.index'));
        }

        $this->pvmGroupRepository->delete($id);

        Flash::success('Pvm Group deleted successfully.');

        return redirect(route('pvmGroups.index'));
    }
}
