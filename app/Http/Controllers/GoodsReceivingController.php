<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Supplier;
use App\Models\Product;
use App\Models\SupplierDocument;
use App\Models\Transaction;
//use App\Repositories\TransactionRepository;

use App\Models\Category;
use App\Models\PvmGroup;
use App\Models\Measure;

use Flash;

class GoodsReceivingController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');

        //$this->middleware('can:view-inventory');
        $this->middleware('can:view-goods-receiving');
    }

    private function inventory_products_select($values){
        $arr = [];
        foreach ($values as $value) {
            $arr[$value->id] = $value->title . ' ' . $value->sku . ' ' . $value->barcode;
        }
        return $arr;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suppliers = Supplier::pluck('title', 'id')->toArray();
        $products = Product::all();
        $products = $this->inventory_products_select($products);
        //$products = Product::pluck('title', 'id')->toArray();

        //SESSION
        $supplier_document_id = session('supplier_document_id');


        $supplier_document = SupplierDocument::find($supplier_document_id);
        $document_processing = false;
        if($supplier_document){
            $document_processing = true;
        }

        //--------------------- new product view --------------------------
        //$categories = Category::all();
        //$cat_select = get_select_categories($categories, []);
        $categories = Category::where('parent_id', 0)->orderBy('parent_id', 'asc')->get();

        $pvm_groups = PvmGroup::pluck('title', 'id');
        $measures = Measure::pluck('title', 'id');

        $data = [
            'suppliers' => $suppliers,
            'products' => $products,
            'supplier_document' => $supplier_document,
            'document_processing' => $document_processing,

            //'categories' => $cat_select,
            'categories' => $categories,
            'pvm_groups' => $pvm_groups,
            'measures'  => $measures
        ];

        //------------------------------------------------------------

        return view('admin.goods_receiving.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $find_doc = SupplierDocument::where('nr', $request->nr)->first();

        if (empty($find_doc->nr)){
            $supplier_document = new SupplierDocument;
            $supplier_document->supplier_id = $request->supplier_id;
            $supplier_document->nr = $request->nr;
            $supplier_document->save();            
        }


        Flash::success('Inventory saved successfully.');

        return redirect()->back()->withInput(
            $request->except('product_id', 'count', 'price')
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($transaction_id)
    {
        $transaction = Transaction::find($transaction_id);

        if (empty($transaction)) {
            Flash::error('Product not found');
            return redirect(route('pajamavimas.index'));
        }

        $transaction->delete($transaction_id);

        Flash::success('Product deleted successfully.');

        //return redirect(route('transactions.index'));
        return redirect(route('pajamavimas.index'));
    }
}
