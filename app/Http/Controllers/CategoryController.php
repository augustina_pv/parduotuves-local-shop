<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Repositories\CategoryRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

use App\Models\Category;
use Storage;

class CategoryController extends AppBaseController
{
    /** @var  CategoryRepository */
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepo)
    {
        $this->middleware('auth');

        $this->middleware('can:view-categories');

        $this->categoryRepository = $categoryRepo;
    }


    /**
     * Display a listing of the Category.
     *
     * @param Request $request
     * @return Response
     */  

    public function index(Request $request)
    {
        $this->categoryRepository->pushCriteria(new RequestCriteria($request));
        $categories = $this->categoryRepository->orderBy('ordering', 'asc')->get()->where('parent_id', 0);

        $data = [
            'categories' => $categories,
        ];

        return view('admin.categories.index', $data);
    }
    

    /**
     * Show the form for creating a new Category.
     *
     * @return Response
     */
    public function create()
    {

        $categories = $this->categoryRepository->orderBy('ordering', 'asc')->get()->where('parent_id', 0);


        $data = [
            'parent_id' => 0,
            'categories' => $categories,
        ];

        return view('admin.categories.create', $data);
    }

    /**
     * Store a newly created Category in storage.
     *
     * @param CreateCategoryRequest $request
     *
     * @return Response
     */
    public function store(CreateCategoryRequest $request)
    {
        $category = new Category;

        //------------------- save -------------------------------
        if($request->title)
            $category->title = $request->title;

        if($request->parent_id)
            $category->parent_id = $request->parent_id;

        if($request->ordering)
            $category->ordering = $request->ordering;

        if($request->image){

            if( !empty($category->image) ){
                //DELETE FILE
            }

            $category->image = store_image_on_disk($request, 'image', 'categories'); //return image path
        }
        $category->save();
        //-----------------------------------------------------


        Flash::success('Category saved successfully.');

        return redirect(route('categories.index'));
    }

    /**
     * Display the specified Category.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $category = $this->categoryRepository->findWithoutFail($id);

        if (empty($category)) {
            Flash::error('Category not found');

            return redirect(route('categories.index'));
        }

        return view('admin.categories.show')->with('category', $category);
    }


    /**
     * Show the form for editing the specified Category.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $category = $this->categoryRepository->findWithoutFail($id);

        if (empty($category)) {
            Flash::error('Category not found');

            return redirect(route('categories.index'));
        }

        $categories = $this->categoryRepository->orderBy('ordering', 'asc')->get()->where('parent_id', 0);

        $data = [
            'category' => $category,
            'parent_id' => $category->parent_id,
            'categories' => $categories,
        ];

        return view('admin.categories.edit', $data);
    }

    /**
     * Update the specified Category in storage.
     *
     * @param  int              $id
     * @param UpdateCategoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategoryRequest $request)
    {
        $category = Category::find($id);

        if (empty($category)) {
            Flash::error('Category not found');

            return redirect(route('categories.index'));
        }

        //------------------- update -------------------------------
        if($request->title)
            $category->title = $request->title;

        //if($request->parent_id)
            $category->parent_id = $request->parent_id or 0;

        if($request->ordering)
            $category->ordering = $request->ordering;

        if($request->image){

            if( !empty($category->image) ){
                //DELETE FILE
            }

            $category->image = store_image_on_disk($request, 'image', 'categories'); //return image path
        }
        $category->update();
        //-----------------------------------------------------


        Flash::success('Category updated successfully.');

        //return redirect(route('categories.index'));
        return redirect()->back();
    }

    /**
     * Remove the specified Category from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $category = $this->categoryRepository->findWithoutFail($id);

        if (empty($category)) {
            Flash::error('Category not found');

            return redirect(route('categories.index'));
        }

        $this->categoryRepository->delete($id);

        Flash::success('Category deleted successfully.');

        //USER ACTIVITY
        add_user_activity($category, 'DELETED');        

        return redirect(route('categories.index'));
    }


    public function trash(){

        $categories = Category::onlyTrashed()->get();

        $data = [
            'categories' => $categories,
        ];
        return view('admin.trash.categories.index', $data);
        
    }

    public function restore($id){

        $category = Category::withTrashed()->find($id);
        $title = $category->title;
        $category->restore();

        Flash::success('Kategorija <strong>'.$title.'</strong> atkurta sėkmingai.');

        //USER ACTIVITY
        add_user_activity($category, 'RESTORED');        

        return redirect()->back();
        
    }

}
