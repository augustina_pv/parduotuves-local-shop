<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use Flash;

class InventoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('can:view-inventory');
    }

    public function index(){

    	$products = Product::all();
    	$total_count = Product::sum('count');
    	//$total_price = Product::sum('price');
    	$total_price = $products->reduce(function($sum, $product){
    		return $sum + $product->price_total;
    	});
    	$total_price = number_format($total_price, 2);

        $inventory_activity_id = session('inventory_activity_id'); //neuzbaigta

    	$data = [
    		'products' => $products,
    		'total_count' => $total_count,
    		'total_price' => $total_price,
            //'inventory_activity_id' => $inventory_activity_id
    	];

    	return view('admin.inventory.inventory', $data);
    }

    public function edit($id){
    	$product = Product::find($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('inventory.inventory'));
        }  

        return view('admin.inventory.edit', compact('product'));  	
    }

    public function update($id, Request $request){
        $product = Product::find($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('inventory.index'));
        }

        $product->count = $request->count;
        $product->update();

        Flash::success('Produktas atnaujintas sėkmingai.');

        return redirect()->back();
    }
}
