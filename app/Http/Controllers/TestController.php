<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use App\Models\SupplierDocument;
use App\Models\Transaction;
use App\Models\Category;
use App\Models\Product;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\UserActivity;
use App\Models\InventoryActivitiesItem;
use Auth;
use Storage;


class TestController extends Controller
{


    public function test_discounts(){

        $price = $this->get_discounted_price(1.20, 1.00, 10);
        var_dump($price);
    }


    private function get_discounted_price($original_price, $price_discounted, $discount_percentage){

        
        $price = $original_price;

        if($price_discounted){
            $price = $price_discounted;
        }
        else{
            if($discount_percentage){
                $discount = ( $price * $discount_percentage ) / 100;
                $new_price = $price - $discount;
                $new_price = round($new_price, 2);
                $price = $new_price;                    
            }            
        }


        return $price;      
    }      

    public function test_activity(){
        $inventory_activity_item = new InventoryActivitiesItem;
        $inventory_activity_item->inventory_activity_id = 1;
        $inventory_activity_item->product_id = 1;
        $inventory_activity_item->count = 1;
        $inventory_activity_item->count_real = 1;
        $saved = $inventory_activity_item->save();         
    }

    public function test_app(){
        var_dump("expression");
        var_dump(env('APP_URL') );
        var_dump(Storage::url(''));
        var_dump(Storage::disk('users'));
        echo '<br>';

        $url = env('APP_URL').'/storage';
        $env = env('APP_ENV').'/env';
        $pvm = env('PVM', 'PVM').'/pvm';

        echo $url.'<br>';
        echo $env.'<br>';
        echo $pvm.'<br>';
        //return view('test');
    }

    public function test_trashed_products(){
        

        //$product = Product::onlyTrashed()->find(2);

        $product = get_trashed_object(2, Product::class);


        var_dump($product);
    }


    public function test_user_activities(){

        $product = Product::find(11);
        echo $product->title;

        $activities = $product->user_activities;

        var_dump($activities);

        $activity = new UserActivity;
        $activity->user_id = Auth::id();
        $activity->status = 'DELETED';

        $product->user_activities()->save($activity);

    }

    public function test_pajam_counts(){
        $supplier_document_id = 2;
        $supplier_document = SupplierDocument::find($supplier_document_id);
        foreach ($supplier_document->transactions as $transaction) {

            $product_id = $transaction->product->id;
            $add_product_count = $transaction->count;

            $product = Product::find($product_id);
            $product->count += $add_product_count;
            $product->save();

            echo $transaction->product->id.' '.$transaction->product->title.' - '.$transaction->count.'</br>';
        }
    }

    public function get_test(){
        echo 'get test from route';
    }

    public function post_test(){
        echo 'post test from route';
    }

    public function ins_transaction(){
    	var_dump("start testing");

    	$user_id = Auth::id();
    	//var_dump($user_id);

    	$document_id = 1;
    	$type_id = 1;
    	$product_id = 1;
    	$count = 3;
    	$price_purchase = '10.00';
    	$price_sale = '12.00';

    	//$id = $this->insert_transaction($document_id, $type_id, $product_id, $count, $price_purchase, $price_sale);
    	var_dump($id);
    }

    private function insert_transaction($document_id, $type_id, $product_id, $count, $price_purchase, $price_sale){

        $user_id = Auth::id();

        $supplier_document = SupplierDocument::find($document_id);
        echo 'SupplierDocument: '.$supplier_document->id.'</br>';


        $transaction = new Transaction;
        
        //$transaction->document_id = $document_id;
        //$transaction->document_type = 'App\Models\SupplierDocument'; //pirkimas
        $transaction->type_id = $type_id;
        $transaction->product_id = $product_id;
        $transaction->count = $count;
        $transaction->price_purchase = $price_purchase;
        $transaction->price = $price_sale;
        $transaction->user_id = $user_id;
        //$transaction->save();

        $supplier_document->transactions()->save($transaction);
        
        return $transaction->id;
    }    
}
