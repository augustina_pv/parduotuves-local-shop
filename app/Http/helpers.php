<?php

if (! function_exists('get_trashed_object')) {

    function get_trashed_object($id, $model){

        $result = $model::onlyTrashed()->find($id);

        return $result;
    }
}


if (! function_exists('add_user_activity')) {

    function add_user_activity($model, $status){

        $logged_user_id = Auth::id();

        $activity = new App\Models\UserActivity;
        $activity->user_id = $logged_user_id;
        $activity->status = $status;
        $model->user_activities()->save($activity);
    }
}


if (! function_exists('object_to_array_for_select')) {

    function object_to_array_for_select($values, $name='title'){
        $arr = [];
        foreach ($values as $value) {
            $arr[$value->id] = $value->$name;
        }
        return $arr;
    }
}


if (! function_exists('get_select_categories')) {

    function get_select_categories($categories, $product_categories){
        $cat_select = [];
        foreach ($categories as $category) {

            $selected = false;
            foreach ($product_categories as $active_category) {
                if($category->id === $active_category->id)
                    $selected = true;
            }
            
            $cat_select[] = [
                'id' => $category->id,
                'title' => $category->title,
                'selected' => $selected
            ];            
        }
        return $cat_select;      
    }
}


if (! function_exists('store_image_on_disk')) {

    function store_image_on_disk($request, $input_name, $disk_name){

        //$path = $request->file($input_name)->store($disk_name);

        $file = Storage::disk($disk_name)->put('', $request->file($input_name));


        return $disk_name.'/'.$file;
    }

}


if (! function_exists('get_price_pvm')) {

    function get_price_pvm($price){
     
        $pvm_rate = env("PVM_RATE");
        $pvm = $price * $pvm_rate / 100;
        $price_pvm = $price + $pvm;

        return round($price_pvm, 2);
    }
}

/*
if (! function_exists('get_price_pvm')) {

    function get_product_price_pvm($product){
     
        $pvm_rate = env("PVM_RATE");
        $pvm = $price * $pvm_rate / 100;
        $price_pvm = $price + $pvm;

        return round($price_pvm, 2);
    }
}
*/