<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PermissionPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    private function permission_check($user, $name){
        $permission = $user->permissions->where('name', $name)->first();

        if (!empty($permission)){
            return true;
        }

        return false;        
    }  

    

    //------------------ inventory (pajamavimas) -----------------
    
    public function view_inventory(User $user){
        return $this->permission_check( $user, 'view-inventory' );
    } 
    
    public function view_goods_receiving(User $user){
        return $this->permission_check( $user, 'view-goods-receiving' );
    }     
    //------------------------------------------------------------

    
    //------------------ transactions & types --------------------
    public function view_transactions(User $user){
        return $this->permission_check( $user, 'view-transactions' );
    }  

    public function view_types(User $user){
        return $this->permission_check( $user, 'view-types' );
    }  
    //-------------------------------------------------------------
   

    //------- products & categories & pvm groups & measures -------
    public function view_products(User $user){
        return $this->permission_check( $user, 'view-products' );
    }

    public function view_categories(User $user){
        return $this->permission_check( $user, 'view-categories' );
    }    

    public function view_pvm_groups(User $user){
        return $this->permission_check( $user, 'view-pvm-groups' );
    }       

    public function view_measures(User $user){
        return $this->permission_check( $user, 'view-measures' );
    }     
    //------------------------------------------------------------



    //--------------------- suppliers & documents ------------------------
    public function view_suppliers(User $user){
        return $this->permission_check( $user, 'view-suppliers' );
    }      

    public function view_supplier_documents(User $user){
        return $this->permission_check( $user, 'view-supplier-documents' );
    }    
    //--------------------------------------------------------------------


    //------------------- customers & receipts & orders ---------------------------
    public function view_customers(User $user){
        return $this->permission_check( $user, 'view-customers' );
    }       

    public function view_receipts(User $user){
        return $this->permission_check( $user, 'view-receipts' );
    }  

    public function view_orders(User $user){
        return $this->permission_check( $user, 'view-orders' );
    }  
    //--------------------------------------------------------------------


    //--------------------- inner documents ------------------------------
    public function view_inner_documents(User $user){
        return $this->permission_check( $user, 'view-inner-documents' );
    }  
    //--------------------------------------------------------------------


    //----------- users & roles & permissions ----------------------------
    public function view_users(User $user){
        return $this->permission_check( $user, 'view-users' );
    }   

    public function view_roles(User $user){
        return $this->permission_check( $user, 'view-roles' );
    }     

    public function view_permissions(User $user){
        return $this->permission_check( $user, 'view-permissions' );
    }    
    //-------------------------------------------------------------------


    //------------------------ reports ----------------------------------
    public function view_reports(User $user){
        return $this->permission_check( $user, 'view-reports' );
    }      
    //-------------------------------------------------------------------
    
}
