<?php

namespace App\Repositories;

use App\Models\Measure;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class MeasureRepository
 * @package App\Repositories
 * @version November 23, 2017, 9:55 am UTC
 *
 * @method Measure findWithoutFail($id, $columns = ['*'])
 * @method Measure find($id, $columns = ['*'])
 * @method Measure first($columns = ['*'])
*/
class MeasureRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Measure::class;
    }
}
