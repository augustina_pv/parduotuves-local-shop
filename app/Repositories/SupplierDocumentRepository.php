<?php

namespace App\Repositories;

use App\Models\SupplierDocument;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class SupplierDocumentRepository
 * @package App\Repositories
 * @version November 23, 2017, 12:10 pm UTC
 *
 * @method SupplierDocument findWithoutFail($id, $columns = ['*'])
 * @method SupplierDocument find($id, $columns = ['*'])
 * @method SupplierDocument first($columns = ['*'])
*/
class SupplierDocumentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nr',
        'supplier_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SupplierDocument::class;
    }
}
