<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table = 'cart';

    public $appends = ['pvm', 'price_pvm'];

    public function discounts()
    {
    	//cart - gali tureti viena discount ( $cart->discounts->last() )
        return $this->morphMany('App\Models\Discount', 'object');
    } 

    public function pvm_group(){
        return $this->belongsTo('App\Models\PvmGroup');
    }  
    
    public function getPvmAttribute(){

        if($this->pvm_group)
            return $this->pvm_group->value;

        return null;
    }      

    public function getPricePvmAttribute(){

        if($this->pvm){
            $pvm = $this->pvm * $this->price / 100;
            $price_pvm = $this->price + $pvm;
            $price_pvm = round($price_pvm, 2);
            return $price_pvm;            
        }

        return null;
    }         
}
