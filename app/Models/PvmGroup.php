<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PvmGroup
 * @package App\Models
 * @version November 23, 2017, 9:51 am UTC
 *
 * @property string title
 */
class PvmGroup extends Model
{
    use SoftDeletes;

    public $table = 'pvm_groups';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'title', 'pos_option', 'value'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'title' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
