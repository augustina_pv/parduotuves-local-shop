<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class User
 * @package App\Models
 * @version November 23, 2017, 12:41 pm UTC
 *
 * @property string name
 * @property string lastname
 * @property string email
 * @property integer role_id
 * @property string image
 */
class User extends Model
{
    use SoftDeletes;

    public $table = 'users';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'lastname',
        'email',
        'role_id',
        'image',
        'password',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'lastname' => 'string',
        'email' => 'string',
        'role_id' => 'integer',
        'image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


    public function role(){
        return $this->belongsTo('App\Models\Role');
    }

    public function permissions(){
        return $this->belongsToMany('App\Models\Permission', 'user_permission');
    }    

    
}
