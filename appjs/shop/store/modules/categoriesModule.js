const categoriesModule = {

	namespaced: true,

	state: {
		parent: 0,
		categories: [],
	},

	/*
	category: {
		id
		title
		parent_id
		image
		ordering
		created_at - nenaud.
		deleted_at - nenaud.
		updated_at - nenaud.
	},
	*/

	getters: {
		parent(state){
			return state.parent
		},

		categories(state){
			return state.categories;
		}
	},

	mutations: {
		fetch_categories(state, categories){
            state.categories = categories;
		},
		set_parent(state, parent){
			state.parent = parent;
		}
	},

	actions: {
		fetch_categories(context, parent=0){
			return axios.get('get_categories/' + parent).then((res) => {
				context.commit('fetch_categories', res.data);
				context.commit('set_parent', parent);
			})
			.catch(error => {
	            console.log('Klaida, bandykite dar kartą');
	            console.log(error);
	        }); 			
		}
	}
}


export default categoriesModule