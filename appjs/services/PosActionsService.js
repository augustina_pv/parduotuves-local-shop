import axios from 'axios'

class PosActionsService{

	constructor() { 
        this.timeout = 1000;
		this.run = false;
		this.found = false;
        this.timer = null;
        this.message = '';
	}

    set_message(){
        this.message = 'Operacija sėkminga';
    }

    pos_action_update_status(on_success, callback){

    	var app = this;
        var data = {
            status_check: 'OKPOS',
            status_change: 'OK',
        }
        axios.get('pos_action_change_status_success', {params: data}).then((res) => {
            //console.log('success status: '+res.data.status);

            if(res.data.status == 0){
            	app.found = false;
                //console.log('status: 0');
            }
            if(res.data.status == 'OK'){
            	app.found = true;
            	console.log(res.data.action); 

                app.set_message();
                on_success();
            }
            callback();
        })
        .catch(error => {
            console.log(error);
            callback();
        }); 
    }

    start(on_success){
    	//console.log('ping');
    	var app = this;
    	this.run = true;

    	this.timer = setInterval(function(){
    		
    		if(app.run){
    			app.run = false;
    			//console.log('ping');
    			app.pos_action_update_status(on_success, function(){
                    //console.log('app.run=true');
                    app.run = true;
                });

    			
    		}
    		
    	}, app.timeout);
    }

    finish(){
        this.run = false;
        clearInterval(this.timer);
    }

}

export default PosActionsService