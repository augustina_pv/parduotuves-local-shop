const shopApi = {
	
	test(){
		console.log('test shop api');
	},

    api_post_send(action, data){
        
        axios.post(action, data).then((res) => {
            //console.log(res);
            console.log('make sync!');
        })
        .catch(error => {
            console.log('Klaida, bandykite dar kartą!!!');
            console.log(error);

            this.api_post_send(action, data); //repeat
        });
    }, 


	//------------- GET -------------------	
	//---main
    get_user_id(callback, user_id){
        return axios.get('get_user_id').then((res) => {
			callback(res.data);		
        })
        .catch(error => {
        	console.log('Klaida, bandykite dar kartą');
            console.log(error);
        });
    },
    //---
	//----------------------------------------

	//------------- POST ---------------------
	//---cart
	save_to_cart(cart_product){
		const data = { product: cart_product }
		this.api_post_send('save_to_cart', data);
	},

	delete_product_from_cart(data){
		this.api_post_send('delete_product_from_cart', data);
	},
	//----

	//--- checkout
	save_orders(order, callback){
        axios.post('save_order', order).then((res) => {
            callback(res);
        })
        .catch(error => {
            console.log('Klaida, bandykite dar kartą');
            console.log(error);
        });
	},
	//---


	//----------------------------------------


	//------------- DELETE -------------------	
	empty_cart(callback){
        axios.delete('empty_cart').then((res) => {
           callback(res);
        })
        .catch(error => {
        	console.log('Klaida, bandykite dar kartą');
            console.log(error);
        });         
	}
	//----------------------------------------
}

export default shopApi;