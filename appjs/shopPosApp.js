window.Vue = require('vue');

import PosActionsService from './services/PosActionsService' 
Vue.prototype.$PosActionsService = new PosActionsService();

//------ import: shop(store;components) ------------------
require('./shoppos/bootstrap');
import store from './shoppos/store/index' 
Vue.component('shop-pos', require('./shoppos/components/ShopPosComponent.vue'));
Vue.component('pos-actions-summary', require('./shoppos/components/PosActionsSummary.vue'));
Vue.component('keyboard', require('./shoppos/components/KeyboardComponent.vue'));
Vue.component('keyboard-number', require('./shoppos/components/KeyboardNumberComponent.vue'));
//-------------------------------------------------------------------------


const shoppos = new Vue({
    el: '#shoppos', 
    delimiters: ['<%','%>'], 
    store,
    mixins : [],
    mounted() {
        var app = this;
        this.$store.dispatch('posActions/fetch_actions');

        //START SERVICES
        this.$PosActionsService.start(function(){
            app.action_message = app.$PosActionsService.message;
            app.action_message_active = true;
            setTimeout(function(){
                app.action_message = '';
                app.action_message_active = false;
                app.$store.dispatch('posActions/fetch_actions');
            }, 5000);
        });
        //------------------------------------------- 
    },    
    data: {
        action_message: '',
        action_message_active: false,
        //message: '',

    },
    computed:{     

    },
    watch: {
    },
    methods: {
        testas(){
            console.log('testas'); 
        },        
        test_click(){
            this.testas();
        },         

        //---------- shop-pos -------------------
        on_shop_pos_insert_action_click(data, callback, msg) {
            console.log(data);
            var app = this;
            this.action_message = msg;
            this.action_message_active = true;

            axios.post('pos_action_insert', data).then((res) => {
                console.log(res.data);
                if(res.data.status === 'PROCESSING'){
                    callback; 
                    //app.$store.dispatch('posActions/fetch_actions');         
                }
            })
            .catch(error => {
                console.log(error);
            });
        }
        //----------------------------------------
    },


});