/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

window.Vue = require('vue');
import shopApi from './api/shopApi' 
import PosActionsOrdersService from './services/PosActionsOrdersService' 
Vue.prototype.$PosActionsOrdersService = new PosActionsOrdersService();

//------ import: shop(store;components) ------------------
require('./shop/bootstrap');
import store from './shop/store/index' 
import test from './shop/mixins/test' 
import shop_helpers from './shop/mixins/helpers' 
Vue.component('breadcrumb', require('./shop/components/BreadcrumbComponent.vue'));
Vue.component('categories', require('./shop/components/CategoriesComponent.vue'));
Vue.component('products', require('./shop/components/ProductsComponent.vue'));
Vue.component('cart', require('./shop/components/CartComponent.vue'));
Vue.component('calculator', require('./shop/components/CalculatorComponent.vue'));
Vue.component('checkout', require('./shop/components/CheckoutComponent.vue'));
//-------------------------------------------------------------------------

//Axios plugin that intercepts failed requests and retries them whenever possible.
//import axiosRetry from 'axios-retry';
//axiosRetry(axios, { retries: 3 });

//-------- axios intercept response errors --------------
axios.interceptors.response.use(function (response) {
    // Do something with response data
    return response;
  }, 
  function (error) {
    const status = error.response ? error.response.status : null
    var config = error.config;
    console.log(config);
    console.log(status);
    error.config.__isRetryRequest = false;

    if (status === 500) { //500 Internal Server Error
        console.log('kartoti');
        error.config.__isRetryRequest = true;
        return axios(error.config);
    }

    if (status === 419) { //authorization
        window.location.href = "login";
    }

    if (status === 401) { //authorization
        window.location.href = "login";
        //axios(config);
        //axios.request(config);
    } 
    return Promise.reject(error);
  });
//----------------------------------------


const shop = new Vue({
    el: '#shop',
    delimiters: ['<%','%>'], 
    store,
    mixins : [test, shop_helpers],
    mounted() {
        console.log('Test App started');
        
        var app = this;
        this.$store.dispatch('categoriesList/fetch_categories'); //api
        this.$store.dispatch('productsList/fetch_products'); //api
        
        shopApi.get_user_id((user_id)=>{
            console.log('User ID: ' + user_id);
            app.user_id = user_id;
            app.$store.dispatch('cart/fetch_cart_products', user_id); //api
        }); //API        

        var app = this;
        $('#payment-modal-id').on("hidden.bs.modal", function(){
            //console.log('hide modal');
            app.checkout_window_full();
        });

        //START SERVICES
        this.$PosActionsOrdersService.start(function(){
            app.message_after_order = app.$PosActionsOrdersService.message;
            setTimeout(function(){
                app.message_after_order = '';
                app.reload_screen();
            }, 5000);
        });      
        //-------------------------------------------
    },    
    data: {
        user_id: 0,
        order_processing: false,
        message_after_order: ''
    },
    computed:{
        search:{
            get(){
                return this.$store.getters['productsList/search'];
            },
            set(value){
                this.$store.dispatch("productsList/search", value );
            }
        },

        total_products_price(){
            var price_total = 0
            var app = this;
            var products = this.$store.getters['cart/products'];
            //console.log(products);

            products.forEach(function(product){
                var price = app.product_discount_filter(product);
                price_total += product.count * price;
            }); 
            
            return price_total.toFixed(2);
        },         
    },
    methods: {
        testas(){
            console.log('testas'); 
        },        
        test_click(){
            this.testas();
        },

        //--------------- main -------------------------------
        /*
        redirect_if_not_logged(){
            var app = this;
            shopApi.get_user_id((user_id)=>{
                console.log('User ID: ' + user_id);

                app.user_id = user_id;
                if(user_id === 0){
                    //window.location.href = "login";
                }
            }); //API
        }, 
        */

        clear_search(){
            this.search = '';
        },

        reload_screen(){
            var app = this;
            shopApi.empty_cart(function(){ //API
                app.$store.dispatch('cart/remove_all_products').then(function(){
                    app.$store.dispatch('categoriesList/fetch_categories'); //api store
                    app.$store.dispatch('productsList/fetch_products'); //api store
                    app.$store.dispatch('cart/fetch_cart_products', app.user_id); //api store
                });
            });
        },

        change_category(category_parent_id){
            if(category_parent_id == 0){ //home
                this.$store.dispatch('breadcrumb/fetch_breadcrumb'); //api
                this.$store.dispatch('categoriesList/fetch_categories'); //api
                this.$store.dispatch('productsList/fetch_products'); //api
            }
            else{
                this.$store.dispatch('breadcrumb/fetch_breadcrumb', category_parent_id);
                this.$store.dispatch('categoriesList/fetch_categories', category_parent_id);
                this.$store.dispatch('productsList/fetch_products_by_category', category_parent_id);                
            }            
        },  

        //---------------------------------------------------

        //---------- breadcrumb ---------------------------
        on_click_go_home(){
            this.$store.dispatch('categoriesList/fetch_categories'); //api
        },
        on_breadcrumb_click(category_id){
            //this.redirect_if_not_logged();
            console.log(category_id);
            this.change_category(category_id);
        },    
        //------------------------------------------------

        //--------- categories --------------------------
        on_category_click(category){
            console.log(category.id);
            this.change_category(category.id);
        },
        //-----------------------------------------------

        //---------- cart -----------------
        //--- cart & products list
        on_add_product(product){
            var app = this;
            console.log('on_add_product');

            this.$store.dispatch('cart/add_product', product).then(()=>{
                //console.log(product_added);
                var cart_product = app.$store.getters['cart/last_added_product'];
                console.log(cart_product);
                shopApi.save_to_cart(cart_product); //API
            });
        },
        //---

        on_remove_product(product){
            console.log('on_remove_product');
            if (product.count === 1)
                shopApi.delete_product_from_cart({product: product}); //API
            else
                shopApi.save_to_cart(product); //API

            this.$store.dispatch('cart/remove_product', product);
        },

        on_cart_item_click(product){ //api
            console.log(product.id);
            var app = this;
            this.$store.dispatch('cart/select_product', product.id).then(function(){
                //var products = app.$store.getters['cart/products'];
                //console.log(products);
            });
        },               
        //---------------------------------

        //---------- calculator --------------
        on_reload_screen(){
            console.log('reload_screen');
            this.reload_screen();
        },
        on_product_discount_percentage(percentage){
            //console.log(percentage);
            var app = this;
            this.$store.dispatch('cart/add_product_discount_percentage', percentage).then(function(){
                //var selected_cart_product = app.$store.getters['cart/selected_product'];
                //console.log(selected_cart_product);
                //shopApi.save_to_cart(selected_cart_product); //API
                var products = app.$store.getters['cart/products'];
                products.forEach(function(product){
                    //console.log(product);
                    shopApi.save_to_cart(product); //API
                });
            });
        },
        on_open_checkout(){
            console.log('open checkout');
            this.$store.dispatch('cart/fetch_cart_products', this.user_id); //api
        },
        //-------------------------------------

        //---------- checkout -------------------
        on_save_order(order) {
            var app = this;
            app.order_processing = true;
            app.checkout_window_small();
            order.products = this.$store.getters['cart/products'];

            shopApi.save_orders(order, function(res){ //API
                if(res.status == 200){
                    app.message_after_order = '<div class="alert alert-success">'+
                                                '<strong>Mokėjimas vykdomas</strong>'+
                                              '</div>';
                    app.order_processing = false;
                }
            });
        },   

        pos_action_change_status(){
            var app = this;
            var data = {
                status_check: 'OKPOS',
                status_change: 'OK',
            }
            axios.get('pos_action_save_order', {params: data}).then((res) => {

                console.log('success status: '+res.data.status);
                
                if(res.data.status == data.status_change){ 
                    console.log(res.data.status);
                }
                else{
                    //app.action_message = 'Operacija NEsėkminga!';
                }
            })
            .catch(error => {
                console.log('Klaida, bandykite dar kartą');
                console.log(error);
            }); 
        },                            
        //----------------------------------------      
    },
    watch: {
    	//cart: {
    	//	handler: function(val, oldVal){
    	//		console.log('cart updated');
    	//	},
    	//	deep: true
    	//}
    }

});