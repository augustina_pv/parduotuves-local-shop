$(document).ready(function(){


	var _token = $('meta[name="csrf_token"]').attr('content');
	var ajax_url = ajax_param.ajax_url;
	//alert(ajax_url);

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': _token
		}
	});


	$(document).on('click', '.null-order', function(){

		var row_obj = $(this).parent().parent().parent();
		var td_status = row_obj.find('td.status');
		var order_id = row_obj.attr('item');


		console.log(order_id);

		null_order_status(order_id, td_status);
	});	


	
	function null_order_status(order_id, td_status){
		$.ajax({
			type: "POST",
			dataType: 'json',
			url: ajax_url,
			data: {
				action: 'null_order_status',
				order_id: order_id
			},
			success: function(data){
				console.log(data);

				if(data.updated){
					td_status.html(data.status);
					td_status.addClass('status-updated');					
				}

			}
		});
	}
		

});	