$(document).ready(function(){
	

	var _token = $('meta[name="csrf_token"]').attr('content');
	var ajax_url = ajax_param.ajax_url;
	//alert(ajax_url);

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': _token
		}
	});

	//-------------------------------------------------------------
	$(document).on('keypress', '#document_nr', function(e){	
	    if(e.which == 13) {
	        process_supplier_click();
	    }
	});	

	$(document).on('click', '#process_supplier', function(){
		process_supplier_click();
	});	

	function process_supplier_click(){
		var document_nr = $('#document_nr').val();
		var supplier_id = $('#supplier_id').val();

		start_process_supplier(document_nr, supplier_id);
			
	}	
	//-----------------------------------------------------------



	//----------------------------------------------------------
	$(document).on('click', '#finish_process_supplier', function(){

		show_alert_confirm("Ar tikrai norite užbaigti šio dokumento operaciją?");
		//finish_process_supplier();	
	});


	$(document).on('click', '#modal-alert-confirm .confirm', function(){

		$('#modal-alert-confirm').modal('toggle'); //close alert confirm
		finish_process_supplier();	
	});	
	//-------------------------------------------------------------
	
	function show_alert_confirm(text){
		$('#modal-alert-confirm .modal-body').html(text);
		$('#modal-alert-confirm').modal('toggle');		
	}

	function show_alert(text){
		$('#modal-alert .modal-body').html(text);
		$('#modal-alert').modal('toggle');		
	}

	
	


	function start_process_supplier(document_nr, supplier_id){

		var is_valid = validate_supplier(supplier_id, document_nr);

		if (is_valid === false){
			//alert('Prašome suvesti tiekėjo informaciją');

			show_alert('Prašome suvesti tiekėjo informaciją');
		}
		else{

			var start_btn = $('#process_supplier');
			var finish_btn = $('#finish_process_supplier');
			var supplier_select = $('#supplier_id');
			var document_nr_input = $('#document_nr');
			var document_status = $('.supplier-document-status');
			var message_obj = $('#product_insert_message');

			message_obj.html('');

			$.ajax({
				type: "POST",
				dataType: 'json',
				url: ajax_url,
				data: {
					action: 'start_process_supplier',
					supplier_id: supplier_id,
					document_nr: document_nr
				},
				success: function(data){
					console.log(data);

					if(data.unique_doc_nr == false){
				        message_obj.html(
				        	'<div class="alert alert-danger">Dokumento Nr jau egzistuoja. Bandykite kitą</div>'
				        ); 
					}
					else{
						if (data.status == 1){ //OK
							supplier_select.attr('disabled', true);
							document_nr_input.attr('disabled', true);
							start_btn.attr('disabled', true);
							finish_btn.attr('disabled', false);
							document_status.addClass('finished');

							$('#products_body').addClass('show');
							$('#product_id').focus();
						} 						
					}

				},
			    error: function(XMLHttpRequest, textStatus, errorThrown) { 
			    	console.log(XMLHttpRequest); 
			        console.log("Status: " + textStatus); 
			        console.log("Error: " + errorThrown);
			        message_obj.html(
			        	'<div class="alert alert-danger">'+textStatus + ' ' + errorThrown+
			        	' try to <a href="pajamavimas">refresh</a></div>'
			        ); 
			    }  				
			});			
		}
	}

	function finish_process_supplier(){

    	//if (confirm("Ar tikrai norite užbaigti šio dokumento operaciją?")) {
    
    		//$('#modal-alert-confirm').modal('toggle'); //close alert confirm

			var start_btn = $('#process_supplier');
			var finish_btn = $('#finish_process_supplier');
			var supplier_select = $('#supplier_id');
			var document_nr_input = $('#document_nr');
			var document_status = $('.supplier-document-status');

			var products_body = $('#products_body');
			var inventory_products = $('#inventory-products table tbody');


			$.ajax({
				type: "POST",
				dataType: 'json',
				url: ajax_url,
				data: {
					action: 'finish_process_supplier',
				},
				success: function(data){
					//alert(data);
					console.log(data);
					if (data.status == 1){ //FINISHED

						supplier_select.attr('disabled', false);
						document_nr_input.attr('disabled', false);
						document_nr_input.val('');
						start_btn.attr('disabled', false);
						finish_btn.attr('disabled', true);
						document_status.removeClass('finished');

						$('#supplier_id').focus();

						products_body.removeClass('show');
						inventory_products.html('');

						window.location.reload(true);
					} 				
				}
			});

		//}	
	}	
	//---------------------------------------------------------------------------------

	//------------- accept only numbers -------------------------------
	$(document).on('keypress', '#count', function(e){
		if(e.which < 48 || e.which > 57)
			e.preventDefault();
	});	


    $('.js-products').on("select2:select", function (e) { 
    	var data = e.params.data;
    	var prod_id = data.id;

		$.ajax({
			type: "POST",
			dataType: 'json',
			url: ajax_url,
			data: {
				action: 'get_product_price',
				prod_id: prod_id
			},
			success: function(data){
				if(data.status == 1){
					$('#product_price_purchace').val(data.price_purchase);
					$('#product_price_sale').val(data.price);
				}
				
			}
		});	

		console.log(data);
		console.log(prod_id);
   	});	


	//------------------------------------------------------------------------

	$(document).on('keypress', '#product_price', function(e){	

		if( e.which < 46 || e.which > 57 ) //numbers & taskas
			e.preventDefault();

	    if(e.which == 13) {
	        insert_inventory_product();
	    }
	});	

	$(document).on('click', '#insert_inventory_product', function(){
		insert_inventory_product();
	});
	//-------------------------------------------------------------------------


	function insert_inventory_product(){
		var product_select = $('#product_id');
		var product_count_select = $('#count');
		var price_purchase_input = $('#product_price_purchace');
		var price_sale_input = $('#product_price_sale');
		var product_title = $('#product_id :selected').text();
		//alert(product_title);

		var is_valid = validate_inventory_product(
				product_select.val(), 
				product_count_select.val(), 
				price_purchase_input.val(),
				price_sale_input.val()
			);

		if (is_valid === false){
			alert('Prašome suvesti produkto informaciją');
		}
		else{

			$.ajax({
				type: "POST",
				dataType: 'json',
				url: ajax_url,
				data: {
					action: 'insert_inventory_product',
					product_id: product_select.val(),
					product_title: product_title,
					count: product_count_select.val(),
					price_purchase: price_purchase_input.val(),
					price_sale: price_sale_input.val()
				},
				success: function(data){
					console.log(data);
					if(data.status === 1){
						//$('#inventory-products').prepend(data.html);
						$('#inventory-products').html(data.html);

						product_count_select.val('');
						price_purchase_input.val('');
						price_sale_input.val('');

						
						$(".js-products").select2("val", "0");
					}
					if(data.status < 0)
						$('#product_insert_message').html(data.message);
				}
			});	
		}
	}


	function validate_supplier(supplier_id, document_nr){
		if ( supplier_id.length < 1 ) 
			return false;
		if ( document_nr.length < 5 ) 
			return false;
		//
		return true
	}


	function validate_inventory_product(product_id, count, price_purchase, price_sale){
		//alert(product_id);
		if ( product_id.length < 1 ) 
			return false;
		if ( count.length < 1 ) 
			return false;
		if ( price_purchase.length < 1 ) 
			return false;
		if ( price_sale.length < 1 ) 
			return false;			
		//
		return true
	}		


	function ajax_test(){
		$.ajax({
			type: "POST",
			dataType: 'json',
			url: ajax_url,
			data: {
				action: 'test',
			},
			success: function(data){
				console.log(data);
			}
		});
	}



});