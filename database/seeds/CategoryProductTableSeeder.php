<?php

use Illuminate\Database\Seeder;

class CategoryProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category_product')->insert([
            [
                'category_id' => 1, //gerimai
                'product_id' => 1, //limonadas
            ],
            [
                'category_id' => 1, //gerimai
                'product_id' => 2, //coca cola
            ],            
            [
                'category_id' => 2, //duona
                'product_id' => 10, //duona bociu
            ],
            [
                'category_id' => 3, //mesa
                'product_id' => 3, //vistiena
            ],
            [
                'category_id' => 3, //mesa
                'product_id' => 11, //kiauliena kiniskai
            ], 
            [
                'category_id' => 4, //kiauliena (mesa)
                'product_id' => 11, //Kiauliena kiniskai
            ], 
            [
                'category_id' => 5, //vistiena (mesa)
                'product_id' => 3, //vistiena
            ],
            [
                'category_id' => 6, //pieno produktai
                'product_id' => 12, //pienas mu
            ],
            [
                'category_id' => 7, //kulinarija
                'product_id' => 5, //pyragas
            ],
            [
                'category_id' => 7, //kulinarija
                'product_id' => 6, //keksas
            ],
            [
                'category_id' => 8, //vaisiai
                'product_id' => 9, //bananai
            ],            
            [
                'category_id' => 7, //darzoves
                'product_id' => 7, //agurkai
            ],                        
            [
                'category_id' => 9, //darzoves
                'product_id' => 8, //pomidorai
            ],  
            [
                'category_id' => 10, //saldumynai
                'product_id' => 4, //snickers
            ],
        ]); 
    }
}
