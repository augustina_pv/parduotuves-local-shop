<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'title' => 'Gėrimai',
                'parent_id' => 0,
                'image' => 'categories/4OAk7IuJgnWgBfurZXEl2u57x2I1fzrPFpKi5t3E.jpeg'
            ],
            [
                'title' => 'Duona',
                'parent_id' => 0,
                'image' => 'categories/jdYyiVuIjOlowY9kcJ3M8ZwKL8CSdW9RYIpWbars.jpeg'
            ],
            [
                'title' => 'Mėsa',
                'parent_id' => 0,
                'image' => 'categories/FtIlBjrXvR0FK8QawL7UOZEvCQjJE0ad6md9hMkV.jpeg'
            ],
            
            [
                'title' => 'Kiauliena',
                'parent_id' => 3,
                'image' => 'categories/mNygzbcEXuMXCiUvPqggMSLIExbWv4AW6aTesazk.jpeg'
            ],   
            [
                'title' => 'Vištiena',
                'parent_id' => 3,
                'image' => 'categories/2MvAW9ldkAD5RYmA90fDWiwuciYmU6Wa4sfHmphN.jpeg'
            ], 
            
            [
                'title' => 'Pieno produktai',
                'parent_id' => 0,
                'image' => 'categories/zX3itMSPmhotqWcB8UvYW5fJ91olZqjQRYgvEN6v.jpeg'
            ],
            [
                'title' => 'Kulinarija',
                'parent_id' => 0,
                'image' => 'categories/DygyLylw26pinqULtA8haGnfw2ZYL8YiwYf6JP8h.jpeg'
            ],
                        [
                'title' => 'Vaisiai',
                'parent_id' => 0,
                'image' => 'categories/ieKovS0DEB2233AoVa5LobKinyQsTppEv3e2mX8O.jpeg'
            ], 
            [
                'title' => 'Dažovės',
                'parent_id' => 0,
                'image' => 'categories/6ie3M7AsqypknaETKKB3Z3wEkIP4VnPV2GrcQi9m.jpeg'
            ], 
            [
                'title' => 'Saldumynai',
                'parent_id' => 0,
                'image' => 'categories/otrC8lJ6SJMrAk5IBySji5FY3LWTDtwcXafrnHJK.jpeg'
            ],             
        ]);      
    }
}
