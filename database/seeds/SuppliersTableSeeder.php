<?php

use Illuminate\Database\Seeder;

class SuppliersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('suppliers')->insert([
            [
                'title' => 'Tiekejas1'
            ],
            [
                'title' => 'Tiekejas2'
            ],
            [
                'title' => 'Tiekejas3'
            ],
            [
                'title' => 'Tiekejas4'
            ]
        ]); 
    }
}
