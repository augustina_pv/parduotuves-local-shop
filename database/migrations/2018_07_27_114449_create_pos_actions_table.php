<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePosActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
/*
        Schema::create('pos_actions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('action', 50)->nullable();
            $table->float('insert')->nullable();
            $table->float('withdraw')->nullable();
            $table->float('refund')->nullable();
            $table->string('message', 200)->nullable();
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
*/

        Schema::create('pos_actions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('action', 50);
            $table->string('value')->nullable();
            $table->string('message', 200)->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pos_actions');
    }
}
