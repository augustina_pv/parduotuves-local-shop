<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransactionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('document_id');
            $table->string('document_type');

            $table->integer('type_id'); //ateityje galvosim ar reikalingas?
            $table->integer('count');
            $table->float('price');
            $table->float('price_purchase')->nullable();

            $table->integer('product_id')->unsigned()->nullable();

            //neleidzia istrinti produkto, jei yra transakcija
            //$table->foreign('product_id')->references('id')->on('products');

            //istrynus produkta, istrina ir transakcija
            //$table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');


            $table->integer('user_id')->unsigned()->nullable();
            //$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');            

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transactions');
    }
}
