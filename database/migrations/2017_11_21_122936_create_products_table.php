<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {

            $table->increments('id');
            $table->string('title', 100);
            $table->string('sku', 100);
            $table->string('barcode', 100)->nullable();
            $table->float('price')->default(0.00);
            $table->float('price_purchase')->default(0.00);
            $table->integer('count')->nullable();
            $table->integer('limit')->nullable();
            $table->string('image')->nullable();

            $table->integer('pvm_group_id')->unsigned()->nullable();
            //$table->foreign('pvm_group_id')->references('id')->on('pvm_groups')->onDelete('cascade');

            $table->integer('measure_id')->unsigned()->nullable();
            //$table->foreign('measure_id')->references('id')->on('measures')->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
