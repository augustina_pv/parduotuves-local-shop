<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryActivitiesItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_activities_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('inventory_activity_id');
            $table->integer('product_id');
            $table->float('price')->nullable();
            $table->integer('count');
            $table->integer('count_real');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_activities_items');
    }
}
