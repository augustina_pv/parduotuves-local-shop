<?php

namespace Tests\Browser;


use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class PajamavimasTest extends DuskTestCase
{


    public function testPajamavimasLogin()
    {

        $this->browse(function ($browser) {
            $browser->visit('/inventory')
                    ->type('email', 'admin@admin.lt')
                    ->type('password', 'passwordas')
                    ->press('Prisijunkite')
                    ->assertPathIs('/inventory');
        });
    }


    public function testPajamavimas()
    {
        
        $this->browse(function (Browser $browser) {

            $browser->visit('/inventory')
                    ->assertSee('Inventorizacija')
                    ->assertSee('Tiekėjas:')
                    ->assertSee('Dokumento Nr:')
                    ->assertSee('Vykdyti')
                    ->assertSee('Užbaigti');

            $browser->select('supplier_id', '1') //Tiekejas value=1 (Tiekejas1)
                    ->type('document_nr', '111222333')
                    ->press('Vykdyti')          //spaudziam Vykdyti
                    ->assertSee('Produktas:')
                    ->assertSee('Kiekis:')
                    ->assertSee('Kaina:')
                    ->assertSee('Įrašyti')
                    ->assertSee('Naujas Produktas');

            $browser->select('product_id', '1') //Produktas value=1 (Limonadas)
                    ->type('count', '5')        //irasomas Kiekis
                    ->type('price', '5.99')     //irasoma Kaina
                    ->press('Įrašyti')          //spaudziam Irasyti
                    ->assertSee('Limonadas')    //Matom Produkto pavadinima: Limonadas

                    ->select('product_id', '2') //Produktas value=1 (Limonadas)
                    ->type('count', '2')        //irasomas Kiekis
                    ->type('price', '1.99')     //irasoma Kaina
                    ->press('Įrašyti')          //spaudziam Irasyti
                    ->assertSee('Coca Cola');    //Matom Produkto pavadinima: Limonadas                    
                    

            $browser->press('Užbaigti');             //spaudziam Uzbaigti
            $browser->driver->switchTo()->alert()->accept(); //patvirtinam alert - OK

            //---- nematom siu uzrasu
            //$browser->assertDontSee('Produktas:')
            //        ->assertDontSee('Kiekis:')
            //        ->assertDontSee('Kaina:');
                    //->assertDontSee('Įrašyti');
                    //->assertDontSee('Naujas Produktas');

        });  
            
    }
}
