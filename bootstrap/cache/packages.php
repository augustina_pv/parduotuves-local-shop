<?php return array (
  'fideloper/proxy' => 
  array (
    'providers' => 
    array (
      0 => 'Fideloper\\Proxy\\TrustedProxyServiceProvider',
    ),
  ),
  'infyomlabs/adminlte-templates' => 
  array (
    'providers' => 
    array (
      0 => '\\InfyOm\\AdminLTETemplates\\AdminLTETemplatesServiceProvider',
    ),
  ),
  'prettus/l5-repository' => 
  array (
    'providers' => 
    array (
      0 => 'Prettus\\Repository\\Providers\\RepositoryServiceProvider',
    ),
  ),
  'laracasts/flash' => 
  array (
    'providers' => 
    array (
      0 => 'Laracasts\\Flash\\FlashServiceProvider',
    ),
    'aliases' => 
    array (
      'Flash' => 'Laracasts\\Flash\\Flash',
    ),
  ),
  'infyomlabs/laravel-generator' => 
  array (
    'providers' => 
    array (
      0 => '\\InfyOm\\Generator\\InfyOmGeneratorServiceProvider',
    ),
  ),
  'laravel/tinker' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Tinker\\TinkerServiceProvider',
    ),
  ),
  'laravelcollective/html' => 
  array (
    'providers' => 
    array (
      0 => 'Collective\\Html\\HtmlServiceProvider',
    ),
    'aliases' => 
    array (
      'Form' => 'Collective\\Html\\FormFacade',
      'Html' => 'Collective\\Html\\HtmlFacade',
    ),
  ),
  'barryvdh/laravel-debugbar' => 
  array (
    'providers' => 
    array (
      0 => 'Barryvdh\\Debugbar\\ServiceProvider',
    ),
    'aliases' => 
    array (
      'Debugbar' => 'Barryvdh\\Debugbar\\Facade',
    ),
  ),
  'laravel/dusk' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Dusk\\DuskServiceProvider',
    ),
  ),
);